import React from "react";

import { RouteProps } from "react-router-dom";

import {
  Login,
  ForgotPassword,
  ForgotPasswordNotification,
  ResetPassword,
  ResetPasswordNotification,
  UserList,
  TemperatureManagement,
  ClassManagement,
} from "screens";
import { MainLayout, DashboardLayout } from "layouts";

export interface IRoute extends RouteProps {
  restricted?: boolean;
  layout?: React.ComponentType;
}

interface IConfigRoutes {
  appRoutes: IRoute[];
  isPrivate?: boolean;
  layout?: React.ComponentType;
}

const privateRoutes: IRoute[] = [
  {
    children: <TemperatureManagement />,
    path: "/dashboard",
    layout: DashboardLayout,
  },
  {
    children: <UserList />,
    path: "/user-list",
    layout: DashboardLayout,
  },
  {
    children: <ClassManagement />,
    path: "/class-management",
    layout: DashboardLayout,
  },
];

const publicRoutes: IRoute[] = [
  {
    children: <Login />,
    path: "/login",
    layout: MainLayout,
    restricted: true,
  },
  {
    children: <ForgotPassword />,
    path: "/forgot-password",
    layout: MainLayout,
    restricted: true,
  },
  {
    children: <ForgotPasswordNotification />,
    path: "/forgot-password-notification",
    layout: MainLayout,
    restricted: true,
  },
  {
    children: <ResetPassword />,
    path: "/reset-password",
    layout: MainLayout,
    restricted: true,
  },
  {
    children: <ResetPasswordNotification />,
    path: "/reset-password-notification",
    layout: MainLayout,
    restricted: true,
  },
];

export const configRoutes: IConfigRoutes[] = [
  {
    appRoutes: privateRoutes,
    isPrivate: true,
    layout: MainLayout,
  },
  {
    appRoutes: publicRoutes,
    layout: MainLayout,
  },
];
