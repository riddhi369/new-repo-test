import * as React from "react";

function PDFIcon(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      {...props}
    >
      <rect width="24" height="24" fill="url(#pattern0)" />
      <defs>
        <pattern
          id="pattern0"
          patternContentUnits="objectBoundingBox"
          width="1"
          height="1"
        >
          <use xlinkHref="#image0_130_2275" transform="scale(0.03125)" />
        </pattern>
        <image
          id="image0_130_2275"
          width="32"
          height="32"
          xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAACCElEQVRYCeWXPUvDUBSG+xO6mIouDaJTFZykOOjo0kk6dRGc3NycdNKljg4KLjp0EyzdS7NKya6diosdBUm/2yvPLSmXNJiPXsRi4ZL7kZz3Oe85DW0i8dc+Ip1MCjNlCdMQc4ymSC8dxcoN8WHhsNxufQjHcSIPnutfXnyK9RVHmMZDZAiy7tj1yMJeWGKIjdV2ZAgAvMHirmNB6AQAPDJEWIBu5Vk26XhzTQT1i4SgJ8I0ZliAUT43yc40RK/0GFi23v3tOw0e2JRRALAYkP71VSAALhFbG8Dg9ER0raoMisVhGlUrAOLUf7y7HUocQK0ArqVhmtB1RysAjTc4P5M9QDlckZ+uWgGwntq3G2+yFKODPQnDPkLD48LMG1ULAN9/gmM9mbuCALjvA658O9hTHZkLAGHEEAWAQRloRlzo3d1MnEA4n5Nz7tECQK0RJjNEAXGzVQXcc94LAKtnzGM7wIMEJLCcW9WZ4F4xv3VsABzgYTL3y8xPzG8vNoBfsDh7iw+QyWQEo1gsyh5gns1mRaVSkfvqmZ9DczuAQKvVkmIIsG40GhKCuZ+ouqcFAEFXTL0yZ+CGKqrOtQBgealUmpbAtu3fdUDNiIwBqtVqU1fUc+98bge8AaOuFwUgZXXLT07U7ILu79RfvoRpNIN/E6aTyfHO1it2aR38S0ov7wcC/LsbvgEnvQMeFSSBawAAAABJRU5ErkJggg=="
        />
      </defs>
    </svg>
  );
}

export default PDFIcon;
