import * as React from "react";

function VerticalTilesIcon(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg
      width="11"
      height="38"
      viewBox="0 0 11 38"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <rect
        x="0.5"
        y="0.5"
        width="10"
        height="37"
        fill={props.fill}
        stroke="#CCCCCC"
      />
      <path d="M4 15V23" stroke="#CCCCCC" />
      <path d="M7 15V23" stroke="#CCCCCC" />
    </svg>
  );
}

export default VerticalTilesIcon;
