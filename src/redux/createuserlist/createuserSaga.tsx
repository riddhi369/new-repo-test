import { put, takeLatest, fork, call } from "redux-saga/effects";
import { CreateUserData, ActionType } from "../../models/createuserlist";
import { IResponse } from "../../models";
import { httpStatus } from "configs/httpStatus";
import { createNewUser } from "services/userList";
import { handleSuccesAction, handleErrorAction } from "./createuserActions";
import { history } from "../configureStore";

function* createUserSaga({ payload }: { payload: CreateUserData }) {
  try {
    const response: IResponse = yield call(createNewUser, payload);
    if (response.status === httpStatus.StatusOK) {
      yield put(handleSuccesAction(response));
    } else if (response.status === httpStatus.StatusUnauthorized) {
      yield put(handleErrorAction(response.message || ""));
      history.push("/login");
    } else {
      yield put(handleErrorAction(response.message || ""));
    }
  } catch (error) {
    yield put(handleErrorAction("error"));
  }
}
function* onCreateUserListWatcher() {
  yield takeLatest(ActionType.CREATE_USER as any, createUserSaga);
}

export default [fork(onCreateUserListWatcher)];
