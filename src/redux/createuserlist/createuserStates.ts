import { RootState } from "../rootReducer";

export const selectIsLoading = (state: RootState) => state.createUser.loading;
export const selectCreateUserErrorMessage = (state: RootState) =>
  state.createUser.error;
export const createUserSuccessResponse = (state: RootState) =>
  state.createUser.createUserResponse;
