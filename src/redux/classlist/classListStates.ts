import { RootState } from "../rootReducer";

export const selectIsLoading = (state: RootState) => state.classList.loading;
export const selectErrorMessage = (state: RootState) => state.classList.error;
export const classListDataResponse = (state: RootState) =>
  state.classList.classListResponse;
