import { RootState } from "../rootReducer";

export const selectIsLoading = (state: RootState) =>
  state.departmentList.loading;
export const selectErrorMessage = (state: RootState) =>
  state.departmentList.error;
export const departmentListDataResponse = (state: RootState) =>
  state.departmentList.departmentListResponse;
