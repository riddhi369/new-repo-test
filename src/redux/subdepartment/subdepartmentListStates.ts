import { RootState } from "../rootReducer";

export const selectIsLoading = (state: RootState) =>
  state.subDepartmentList.loading;
export const selectErrorMessage = (state: RootState) =>
  state.subDepartmentList.error;
export const subDepartmentListDataResponse = (state: RootState) =>
  state.subDepartmentList.subDepartmentListResponse;
