import { RootState } from "../rootReducer";

export const selectIsLoading = (state: RootState) =>
  state.activitiesList.loading;
export const selectErrorMessage = (state: RootState) =>
  state.activitiesList.error;
export const activitiesListDataResponse = (state: RootState) =>
  state.activitiesList.activitiesListResponse;
