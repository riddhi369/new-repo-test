import { RootState } from "../rootReducer";

export const selectIsLoading = (state: RootState) =>
  state.resetPassword.loading;
export const selectErrorMessage = (state: RootState) =>
  state.resetPassword.error;
