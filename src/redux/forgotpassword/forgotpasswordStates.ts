import { RootState } from "../rootReducer";

export const selectIsLoading = (state: RootState) =>
  state.forgotPassword.loading;
export const selectErrorMessage = (state: RootState) =>
  state.forgotPassword.error;
