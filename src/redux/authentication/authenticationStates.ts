import { RootState } from "../rootReducer";

export const selectIsLoading = (state: RootState) => state.loginForm.loading;
export const selectErrorMessage = (state: RootState) => state.loginForm.error;
