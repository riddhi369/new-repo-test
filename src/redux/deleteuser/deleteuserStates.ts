import { RootState } from "../rootReducer";

export const selectIsLoading = (state: RootState) => state.updateUser.loading;
export const deleteUserErrorMessage = (state: RootState) =>
  state.deleteUser.error;
export const deleteUserSuccessResponse = (state: RootState) =>
  state.deleteUser.deleteUserResponse;
