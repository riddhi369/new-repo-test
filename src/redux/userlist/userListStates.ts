import { RootState } from "../rootReducer";

export const selectIsLoading = (state: RootState) => state.userList.loading;
export const selectErrorMessage = (state: RootState) => state.userList.error;
export const userListDataResponse = (state: RootState) =>
  state.userList.userListResponse;
