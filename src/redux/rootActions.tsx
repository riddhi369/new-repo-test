import * as AuthenticationActions from "redux/authentication/authenticationActions";
import * as ForgotPasswordActions from "redux/forgotpassword/forgotpasswordActions";
import * as ResetPasswordActions from "redux/resetpassword/resetpasswordActions";
import * as UserListActions from "redux/userlist/userListActions";
import * as ClassListActions from "redux/classlist/classListActions";
import * as DepartmentListActions from "redux/department/departmentListActions";
import * as SubDepartmentListActions from "redux/subdepartment/subdepartmentListActions";
import * as ActivitiesListActions from "redux/activitieslist/activitiesListActions";
import * as CreateUserActions from "redux/createuserlist/createuserActions";
import * as UpateUserActions from "redux/updateuser/updateuserActions";
import * as DeleteUserActions from "redux/deleteuser/deleteuserActions";

export const ActionCreators = Object.assign({}, { ...AuthenticationActions });
export const ForgotPasswordActionCreators = Object.assign(
  {},
  { ...ForgotPasswordActions }
);
export const ResetPasswordActionCreators = Object.assign(
  {},
  { ...ResetPasswordActions }
);
export const UserListActionCreators = Object.assign({}, { ...UserListActions });
export const ClassListActionCreators = Object.assign(
  {},
  { ...ClassListActions }
);
export const DepartmentListActionCreators = Object.assign(
  {},
  { ...DepartmentListActions }
);
export const SubDepartmentListActionCreators = Object.assign(
  {},
  { ...SubDepartmentListActions }
);
export const ActivitiesListActionCreators = Object.assign(
  {},
  { ...ActivitiesListActions }
);
export const CreateUserActionCreators = Object.assign(
  {},
  { ...CreateUserActions }
);
export const UpdateUserActionCreators = Object.assign(
  {},
  { ...UpateUserActions }
);
export const DeleteUserActionCreators = Object.assign(
  {},
  { ...DeleteUserActions }
);
