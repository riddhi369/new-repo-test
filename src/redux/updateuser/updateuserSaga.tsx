import { put, takeLatest, fork, call } from "redux-saga/effects";
import { UpdateUserData, ActionType } from "../../models/updateuser";
import { IResponse } from "../../models";
import { httpStatus } from "configs/httpStatus";
import { updateUser } from "services/userList";
import { handleSuccesAction, handleErrorAction } from "./updateuserActions";
import { history } from "../configureStore";

function* updateUserSaga({ payload }: { payload: UpdateUserData }) {
  try {
    const response: IResponse = yield call(updateUser, payload);
    if (response.status === httpStatus.StatusOK) {
      yield put(handleSuccesAction(response));
    } else if (response.status === httpStatus.StatusUnauthorized) {
      yield put(handleErrorAction(response.message || ""));
      history.push("/login");
    } else {
      yield put(handleErrorAction(response.message || ""));
    }
  } catch (error) {
    yield put(handleErrorAction("error"));
  }
}
function* onUpdateUserListWatcher() {
  yield takeLatest(ActionType.UPDATE_USER as any, updateUserSaga);
}

export default [fork(onUpdateUserListWatcher)];
