import { RootState } from "../rootReducer";

export const selectIsLoading = (state: RootState) => state.updateUser.loading;
export const updateUserErrorMessage = (state: RootState) =>
  state.updateUser.error;
export const updateUserSuccessResponse = (state: RootState) =>
  state.updateUser.updateUserResponse;
