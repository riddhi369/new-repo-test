import { all } from "redux-saga/effects";
import authenticationSagas from "redux/authentication/authenticationSaga";
import forgotPasswordSagas from "redux/forgotpassword/forgotpasswordSaga";
import resetPasswordSagas from "redux/resetpassword/resetpasswordSaga";
import userListSagas from "redux/userlist/userListSaga";
import classListSagas from "redux/classlist/classListSaga";
import departmentListSagas from "redux/department/departmentListSaga";
import subDepartmentListSagas from "redux/subdepartment/subdepartmentListSaga";
import activitiesListSagas from "redux/activitieslist/activitiesListSaga";
import createUserSagas from "redux/createuserlist/createuserSaga";
import updateUserSagas from "redux/updateuser/updateuserSaga";
import deleteUserSagas from "redux/deleteuser/deleteuserSaga";

export default function* startForman() {
  yield all([
    ...authenticationSagas,
    ...forgotPasswordSagas,
    ...resetPasswordSagas,
    ...userListSagas,
    ...classListSagas,
    ...departmentListSagas,
    ...subDepartmentListSagas,
    ...activitiesListSagas,
    ...createUserSagas,
    ...updateUserSagas,
    ...deleteUserSagas,
  ]);
}
