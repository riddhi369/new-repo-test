import { History } from "history";
import { combineReducers } from "redux";
import { routerReducer, RouterState } from "react-router-redux";
import {
  authenticationReducer,
  AuthenticationReducerType,
} from "../redux/authentication/authenticationReducer";
import {
  forgotPasswordReducer,
  ForgotPasswordReducerType,
} from "../redux/forgotpassword/forgotpasswordReducer";
import {
  resetPasswordReducer,
  ResetPasswordReducerType,
} from "../redux/resetpassword/resetpasswordReducer";
import {
  userListReducer,
  UserListReducerType,
} from "../redux/userlist/userListReducer";
import {
  classListReducer,
  ClassListReducerType,
} from "../redux/classlist/classListReducer";
import {
  departmentListReducer,
  DepartmentListReducerType,
} from "../redux/department/departmentListReducer";
import {
  subDepartmentListReducer,
  SubDepartmentListReducerType,
} from "../redux/subdepartment/subdepartmentListReducer";
import {
  activitiesListReducer,
  ActivitiesListReducerType,
} from "../redux/activitieslist/activitiesListReducer";
import {
  createUserReducer,
  CreateUserReducerType,
} from "../redux/createuserlist/createuserReducer";
import {
  updateUserReducer,
  UpdateUserReducerType,
} from "../redux/updateuser/updateuserReducer";
import {
  deleteUserReducer,
  DeleteUserReducerType,
} from "../redux/deleteuser/deleteuserReducer";

export interface RootState {
  loginForm: AuthenticationReducerType;
  forgotPassword: ForgotPasswordReducerType;
  resetPassword: ResetPasswordReducerType;
  userList: UserListReducerType;
  classList: ClassListReducerType;
  departmentList: DepartmentListReducerType;
  subDepartmentList: SubDepartmentListReducerType;
  activitiesList: ActivitiesListReducerType;
  createUser: CreateUserReducerType;
  updateUser: UpdateUserReducerType;
  deleteUser: DeleteUserReducerType;
  routerReducer: RouterState;
}

export default (history: History) =>
  combineReducers({
    loginForm: authenticationReducer,
    forgotPassword: forgotPasswordReducer,
    resetPassword: resetPasswordReducer,
    userList: userListReducer,
    classList: classListReducer,
    departmentList: departmentListReducer,
    subDepartmentList: subDepartmentListReducer,
    activitiesList: activitiesListReducer,
    createUser: createUserReducer,
    updateUser: updateUserReducer,
    deleteUser: deleteUserReducer,
    routerReducer,
  });
