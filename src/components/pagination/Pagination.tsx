import React, { ReactElement } from "react";
import styled from "styled-components";
import { Pagination as OriginalPagination, PaginationProps } from "antd";

import { theme } from "theme/theme";

const Pagination = styled(OriginalPagination)`
  li {
    min-width: 30px;
    height: 30px;
  }
  .ant-pagination-item,
  .ant-pagination-item-active,
  .ant-pagination-next,
  .ant-pagination-prev {
    border-radius: 4px;
    a {
      font-weight: ${theme.fontWeight.bold};
      font-size: 15px;
    }
  }
  .ant-pagination-item,
  .ant-pagination-item-link {
    border-color: ${theme.colors.button};
    background: ${theme.colors.white};
    a {
      color: ${theme.colors.button};
    }
  }
  .ant-pagination-item-link {
    border-radius: 4px;
    .anticon {
      color: ${theme.colors.button};
    }
    .anticon-double-right {
      svg {
        fill: ${theme.colors.button};
      }
    }
  }
  .ant-pagination-item-link:hover {
    .anticon-right,
    .anticon-left {
      color: ${theme.colors.white};
    }
    .anticon-double-right {
      svg {
        fill: ${theme.colors.button};
      }
    }
  }
  .ant-pagination-item-active {
    border-color: #cccccc;
    background: #f2f2f2;
    a {
      color: #b7b7b7;
    }
  }
  .ant-pagination-item:hover:not(.ant-pagination-item-active),
  .ant-pagination-item-link:hover {
    border-color: ${theme.colors.button};
    background: ${theme.colors.button};
    a {
      color: ${theme.colors.white};
    }
  }
  .ant-pagination-prev,
  .ant-pagination-item {
    margin-right: 4px;
  }
  .ant-pagination-item-container {
    .ant-pagination-item-ellipsis {
      color: ${theme.colors.button};
      top: 9px;
    }
  }
  .ant-pagination-jump-prev,
  .ant-pagination-jump-next {
    margin-right: 5px;
    min-width: 20px;
  }
  .ant-pagination-prev {
    margin-right: 5px;
  }
  ${theme.media.desktop} {
    .ant-pagination-item {
      margin-right: 8px;
      margin-left: 0;
    }
    .ant-pagination-prev {
      margin-right: 12px;
    }
    .ant-pagination-next {
      margin-left: 4px;
    }
    .ant-pagination-jump-prev,
    .ant-pagination-jump-next {
      margin-right: 8px;
    }
  }
`;

const CustomPagination: React.FC<PaginationProps> = ({
  ...props
}): ReactElement => {
  return <Pagination {...props} />;
};

export default CustomPagination;
