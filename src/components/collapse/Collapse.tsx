import React from "react";
import { Collapse, CollapseProps } from "antd";
import styled from "styled-components";

import { CarrotIcon } from "icons";

const { Panel } = Collapse;

const CollapseStyled = styled(Collapse)(({ theme }) => ({
  backgroundColor: "unset",
  border: "unset",
  ".ant-collapse-header": {
    fontSize: theme.sizes.lg,
    fontWeight: theme.fontWeight.bold,
    lineHeight: "26px!important",
    color: `${theme.colors.text.primary}!important`,
    display: "flex",
    alignItems: "center",
    padding: "0!important",
  },
  ".ant-collapse-item": {
    borderBottom: "none",
  },
  ".ant-collapse-arrow": {
    order: 2,
    marginRight: 0,
    marginLeft: 22,
  },
  ".ant-collapse-icon-position-right .ant-collapse-header": {
    color: "red",
  },
  ".ant-collapse-content-box": {
    padding: 0,
    paddingTop: "20px!important",
  },
}));

interface ICollapse extends CollapseProps {
  header?: string;
  colorIcon?: string;
}

const CollapseCustom: React.FC<ICollapse> = ({
  header,
  colorIcon = "#3B3B3B",
  children,
  ...rest
}) => {
  return (
    <CollapseStyled
      bordered={false}
      expandIcon={({ isActive }) => (
        <CarrotIcon
          style={{ transform: isActive ? "rotate(0)" : "rotate(-180deg)" }}
          fill={colorIcon}
        />
      )}
      className="site-collapse-custom-collapse"
      {...rest}
    >
      <Panel header={header} key="1" className="site-collapse-custom-panel">
        {children}
      </Panel>
    </CollapseStyled>
  );
};

export default CollapseCustom;
