import React, { useCallback, useLayoutEffect } from "react";

import { Route, useHistory } from "react-router-dom";

import { isLoggedIn } from "utils";
import { IRoute } from "routes/configs";

export interface IRestrictedRouteProps extends IRoute {
  isPrivate?: boolean;
}

const RestrictedRoute: React.FC<IRestrictedRouteProps> = ({
  children,
  restricted = false,
  isPrivate,
  layout: Layout,
  ...props
}) => {
  const history = useHistory();
  const redirect = useCallback(() => {
    if (isPrivate && !isLoggedIn()) {
      history.replace("/login");
    }
    if (restricted && isLoggedIn()) {
      history.replace("/dashboard");
    }
  }, [isPrivate, history, restricted]);

  useLayoutEffect(() => {
    redirect();
  }, [redirect]);

  const Component = Layout ? <Layout>{children}</Layout> : children;

  return <Route {...props}>{Component}</Route>;
};

export default RestrictedRoute;
