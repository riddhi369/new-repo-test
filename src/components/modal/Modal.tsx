import React, { ReactElement } from "react";
import { Modal, ModalProps } from "antd";

import { CloseIcon } from "icons";
import { ModalForm, ModalCustomHeader, ConfirmModal } from "./Modal.style";

type ModalOptions = "form" | "confirm" | "search";

interface IModal extends ModalProps {
  type: ModalOptions;
}

const CustomModal: React.FC<IModal> = ({
  footer,
  type,
  ...props
}): ReactElement => {
  const renderShowCancelCommendNotification = () => {
    switch (type) {
      case "form":
        return <ModalForm {...props} closeIcon={<CloseIcon />} footer={null} />;
      case "confirm":
        return <ConfirmModal {...props} closeIcon={<></>} footer={footer} />;
      case "search":
        return (
          <ModalCustomHeader
            {...props}
            closeIcon={<CloseIcon />}
            footer={null}
          />
        );
    }
  };

  return <>{renderShowCancelCommendNotification()}</>;
};

export default CustomModal;
