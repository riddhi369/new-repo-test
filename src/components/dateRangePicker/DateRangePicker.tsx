import React from "react";
import DatePicker, { registerLocale } from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import ja from "date-fns/locale/ja";

import {
  WrapperDateRangePickerFirst,
  WrapperDateRangePickerSecond,
  Text,
  CalendarIconStyled,
} from "./DateRangePicker.style";

registerLocale("ja", ja);
const dateFormat = "yyyy/MM/dd";
const CustomHeader = ({ monthDate, decreaseMonth, increaseMonth }: any) => {
  return (
    <>
      <button
        aria-label="Previous Month"
        className={
          "react-datepicker__navigation react-datepicker__navigation--previous"
        }
        onClick={decreaseMonth}
      >
        <span
          className={
            "react-datepicker__navigation-icon react-datepicker__navigation-icon--previous"
          }
        >
          {"<"}
        </span>
      </button>
      <div className="react-datepicker__current-month">
        {monthDate.getFullYear()}年{monthDate.getMonth() + 1}月
      </div>
      <button
        aria-label="Next Month"
        className={
          "react-datepicker__navigation react-datepicker__navigation--next"
        }
        onClick={increaseMonth}
      >
        <span
          className={
            "react-datepicker__navigation-icon react-datepicker__navigation-icon--next"
          }
        >
          {">"}
        </span>
      </button>
    </>
  );
};

const DateRangePicker = ({
  startDate,
  endDate,
  setStartDate,
  setEndDate,
}: any) => {
  return (
    <div style={{ display: "inline-flex" }}>
      <WrapperDateRangePickerFirst>
        <div style={{ display: "flex", position: "relative" }}>
          <DatePicker
            dateFormat={dateFormat}
            selected={startDate}
            onChange={(date) => setStartDate(date)}
            selectsStart
            startDate={startDate}
            endDate={endDate}
            renderCustomHeader={CustomHeader}
            highlightDates={[new Date("2014/02/07"), new Date("2014/02/03")]}
            locale="ja"
          />

          <CalendarIconStyled />
        </div>
      </WrapperDateRangePickerFirst>
      <Text>~</Text>
      <WrapperDateRangePickerSecond>
        <div style={{ display: "flex", position: "relative" }}>
          <DatePicker
            dateFormat={dateFormat}
            selected={endDate}
            onChange={(date) => setEndDate(date)}
            selectsEnd
            startDate={startDate}
            endDate={endDate}
            minDate={startDate}
            highlightDates={[new Date("2014/02/07")]}
            locale="ja"
          />
          <CalendarIconStyled />
        </div>
      </WrapperDateRangePickerSecond>
    </div>
  );
};

export default DateRangePicker;
