import React from "react";
import { Checkbox } from "antd";
import styled from "styled-components";

interface ICheckboxItem {
  value?: string;
  key?: number;
  name?: string;
}

interface ICheckbox {
  row?: boolean;
  onChange?: () => void;
  list?: ICheckboxItem[];
}

const CheckboxStyled = styled(Checkbox)(({ theme }) => ({
  alignItems: "unset",
  outline: "none",
  "&:hover": {
    ".ant-checkbox-inner": {
      borderColor: theme.colors.border,
    },
  },
  ".ant-checkbox": {
    top: "unset",
  },
  ".ant-checkbox-inner": {
    width: 20,
    height: 21,
    border: `2px solid ${theme.colors.border}`,
  },
  ".ant-checkbox-checked .ant-checkbox-inner": {
    backgroundColor: theme.colors.button,
    borderColor: `${theme.colors.button}!important`,
  },
  ".ant-checkbox-checked::after": {
    border: "none",
  },
  ".ant-checkbox-input:focus + .ant-checkbox-inner": {
    borderColor: theme.colors.border,
  },
}));

const CheckboxCustom: React.FC<ICheckbox> = ({ row, list, onChange }) => {
  const styles = row ? { display: "flex" } : "";
  const spaceItem = row ? { marginRight: 32 } : { marginBottom: 12 };
  return (
    <Checkbox.Group style={{ width: "100%" }} onChange={onChange}>
      <div style={{ ...styles }}>
        {(list || []).map((item) => (
          <div key={item.key} style={{ ...spaceItem }}>
            <CheckboxStyled value={item.value}>{item.name}</CheckboxStyled>
          </div>
        ))}
      </div>
    </Checkbox.Group>
  );
};

export default CheckboxCustom;
