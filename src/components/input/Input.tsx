import React from "react";
import styled from "styled-components";

import { ErrorMessage } from "theme/CommonStyle";

interface IInput {
  label?: React.ReactNode;
  height?: number;
  width?: number;
  fs?: number;
  fw?: number;
  fsLabel?: number; // font size
  fwLabel?: number; // font weight
  lhLabel?: string; // line height
  value?: string;
  name?: string;
  type?: string;
  onChange?: any;
  error?: string;
  marginForm?: string;
  bdr?: number; // border-radius
}

const FormControl = styled.div<IInput>(({ marginForm }) => ({
  position: "relative",
  margin: marginForm,
}));

const Label = styled.div<IInput>(({ theme, fsLabel, fwLabel, lhLabel }) => ({
  color: theme.colors.text.primary,
  fontSize: fsLabel || theme.sizes.xs,
  fontWeight: fwLabel || 400,
  lineHeight: lhLabel || "12px",
  marginBottom: 8,
}));

const InputStyled = styled.input<IInput>(
  ({ theme, width, height, fs, fw, bdr }) => ({
    height: height || 40,
    width: width || "100%",
    borderRadius: bdr || 6,
    boxShadow: "inset 0px 2px 4px rgba(0, 0, 0, 0.09)",
    border: `1px solid ${theme.colors.border}`,
    fontSize: fs || theme.sizes.sm,
    fontWeight: fw || theme.fontWeight.medium,
    lineHeight: "20px",
    padding: "0px 12px",
    outline: "none",
  })
);

const Input: React.FC<IInput> = ({
  label,
  error,
  fsLabel,
  fwLabel,
  lhLabel,
  marginForm,
  ...rest
}) => {
  return (
    <FormControl marginForm={marginForm}>
      {label && (
        <Label fsLabel={fsLabel} fwLabel={fwLabel} lhLabel={lhLabel}>
          {label}
        </Label>
      )}
      <InputStyled {...rest} spellCheck="false" />
      {error && <ErrorMessage>{error}</ErrorMessage>}
    </FormControl>
  );
};

export default Input;
