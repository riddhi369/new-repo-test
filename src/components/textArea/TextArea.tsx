import React from "react";
import styled from "styled-components";

import { ErrorMessage } from "theme/CommonStyle";

interface ITextArea {
  height?: string;
  label?: string;
  width?: number;
  bdr?: string; // border radius
  fsLabel?: number; // font size
  fwLabel?: number; // font weight
  lhLabel?: string; // line height
  value?: string;
  name?: string;
  type?: string;
  onChange?: any;
  error?: string;
  marginForm?: string;
  placeholder?: string;
}

const FormControl = styled.div<ITextArea>(({ marginForm }) => ({
  position: "relative",
  margin: marginForm,
}));

const Label = styled.div<ITextArea>(({ theme, fsLabel, fwLabel, lhLabel }) => ({
  color: theme.colors.text.primary,
  fontSize: fsLabel || theme.sizes.xs,
  fontWeight: fwLabel || 400,
  lineHeight: lhLabel || "12px",
  marginBottom: 8,
}));

const TextAreaStyled = styled.textarea<ITextArea>(
  ({ theme, width, bdr, height }) => ({
    height: 62 || height,
    width: width || "100%",
    borderRadius: bdr || 6,
    boxShadow: "inset 0px 2px 4px rgba(0, 0, 0, 0.08)",
    border: `1px solid ${theme.colors.textFieldBackground}`,
    fontSize: 14,
    fontWeight: theme.fontWeight.medium,
    lineHeight: "20px",
    padding: "5px 12px 6px",
    outline: "none",
  })
);

const TextAreaCustom: React.FC<ITextArea> = ({
  label,
  error,
  fsLabel,
  fwLabel,
  lhLabel,
  marginForm,
  placeholder,
  ...rest
}) => {
  return (
    <FormControl marginForm={marginForm}>
      {label && (
        <Label fsLabel={fsLabel} fwLabel={fwLabel} lhLabel={lhLabel}>
          {label}
        </Label>
      )}
      <TextAreaStyled
        {...rest}
        spellCheck="false"
        style={{ resize: "none" }}
        placeholder={placeholder}
      />
      {error && <ErrorMessage>{error}</ErrorMessage>}
    </FormControl>
  );
};

export default TextAreaCustom;
