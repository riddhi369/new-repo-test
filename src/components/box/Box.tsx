import React from "react";
import styled from "styled-components";

interface IBox {
  title?: string;
  subTitle?: string;
  padding?: string;
  children?: React.ReactNode;
  action?: React.ReactNode;
}

const Wrapper = styled.div``;

const Title = styled.h1(({ theme }) => ({
  color: theme.colors.text.primary,
  fontWeight: theme.fontWeight.bold,
  fontSize: theme.sizes.md,
  lineHeight: "23px",
  marginBottom: 16,
}));

const SubTitle = styled.h1(({ theme }) => ({
  color: theme.colors.text.primary,
  fontWeight: theme.fontWeight.medium,
  fontSize: theme.sizes.sm,
  lineHeight: "14px",
  marginBottom: 13,
}));

const Container = styled.div<IBox>(({ theme, padding }) => ({
  background: theme.colors.white,
  padding: padding,
  borderRadius: 8,
}));

const WrapperTitle = styled.div`
  position: relative;
`;

const WrapperAction = styled.div`
  position: absolute;
  right: 0;
  bottom: -9px;
`;

const Box: React.FC<IBox> = ({
  title,
  subTitle,
  children,
  padding,
  action,
}) => {
  return (
    <Wrapper>
      <WrapperTitle>
        {title && <Title>{title}</Title>}
        {subTitle && <SubTitle>{subTitle}</SubTitle>}
        {action && <WrapperAction>{action}</WrapperAction>}
      </WrapperTitle>
      <Container padding={padding}>{children}</Container>
    </Wrapper>
  );
};

export default Box;
