import React, { useState, useEffect } from "react";
import { Layout, Menu, Grid, Drawer } from "antd";
import styled from "styled-components";
import { Link, useLocation } from "react-router-dom";
import { useTranslation } from "react-i18next";

import {
  Logo,
  CloseSidebarIcon,
  AvatarIcon,
  UserIcon,
  TemperatureIcon,
  HouseIcon,
  CircleIcon,
  LetterIcon,
  InjectionIcon,
  ContactIcon,
  StudentAssociationIcon,
  QRCodeIcon,
  SettingIcon,
  MenuIcon,
  CloseMenuIcon,
} from "icons";
import { theme } from "theme/theme";

const { Header, Sider, Content, Footer } = Layout;
const { SubMenu } = Menu;
const { useBreakpoint } = Grid;

const WrapperLogo = styled.div`
  ${theme.media.laptopL} {
    width: 200px;
    padding-left: 21px;
    margin-right: 26px;
  }
`;

const HeaderStyled = styled(Header)(({ theme }) => ({
  background: `${theme.colors.white}`,
  boxShadow: "0px 4px 8px rgba(0, 0, 0, 0.16)",
  height: 60,
  width: "100%",
  position: "fixed",
  left: 0,
  paddingLeft: 0,
  paddingRight: 0,
  zIndex: 3,
}));

const LineVertical = styled.div(({ theme }) => ({
  background: theme.colors.background,
  height: 28,
  position: "absolute",
  width: 1,
  left: 0,
  top: 16,
}));

const CloseSidebarText = styled.div(({ theme }) => ({
  fontSize: theme.sizes.md,
  fontWeight: theme.fontWeight.bold,
  marginLeft: 10,
  color: theme.colors.text.secondary,
}));

const CloseSidebarTextMobile = styled.div`
  font-size: ${theme.sizes.sm};
  font-weight: ${theme.fontWeight.bold};
  color: ${theme.colors.text.secondary};
  ${theme.media.tablet} {
    font-size: ${theme.sizes.md};
  }
  ${theme.media.laptopL} {
    display: none;
  }
`;

const Avatar = styled.div(({ theme }) => ({
  verticalAlign: "middle",
  width: 40,
  height: 40,
  borderRadius: "50%",
  background: theme.colors.background,
}));

const Time = styled.div(({ theme }) => ({
  fontSize: theme.sizes.md,
  fontWeight: theme.fontWeight.regular,
  color: theme.colors.text.secondary,
  marginRight: 24,
}));

const SiderStyled = styled(Sider)(({ theme }) => ({
  background: theme.colors.white,
  marginTop: 60,
  borderRight: `1px solid ${theme.colors.border}`,
  overflow: "auto",
  height: "100vh",
  position: "fixed",
  left: 0,
  ".ant-menu": {
    paddingTop: 30,
  },
  ".ant-menu-item": {
    paddingLeft: "22px!important",
  },
  ".ant-menu-submenu-title": {
    paddingLeft: "22px!important",
  },
  ".ant-menu-item-only-child": {
    paddingLeft: "48px!important",
  },
}));

const FlexItem = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

const WrapperHeaderWeb = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 0 10px;
  align-items: center;
  ${theme.media.laptopL} {
    display: flex;
    padding: 0;
    justify-content: unset;
    align-items: unset;
  }
`;

const WrapperLeftHeader = styled.div`
  display: none;
  ${theme.media.laptopL} {
    display: flex;
    justify-content: space-between;
    width: 100%;
    padding: 0 32px 0 12px;
    position: relative;
  }
`;

const MenuIconStyled = styled(MenuIcon)`
  ${theme.media.laptopL} {
    display: none;
  }
`;

const CloseMenuIconStyled = styled(CloseMenuIcon)`
  ${theme.media.laptopL} {
    display: none;
  }
`;

const ContentStyled = styled(Content)`
  padding: 32px 16px 0;
  min-height: 0px;
  background: #f2f2f2;
  ${theme.media.laptopL} {
    padding: 32px 32px 0;
  }
`;

const CopyRight = styled.div`
  text-align: center;
  font-weight: ${theme.fontWeight.medium};
  font-size: ${theme.sizes.xs};
  color: ${theme.colors.icon};
`;

const OpenSidebarIcon = styled(CloseSidebarIcon)`
  cursor: pointer;
  transform: rotate(-180deg);
`;

const FooterStyled = styled(Footer)`
  padding: 16px;
  ${theme.media.desktop} {
    padding: 24px;
  }
`;

interface IItem {
  key: string;
  path: string;
  label?: string;
}

interface IItems {
  key: string;
  path: string;
  label?: string;
  icon?: React.ReactNode;
  children?: IItem[];
}

const MenuItems: IItems[] = [
  {
    key: "1",
    icon: <LetterIcon />,
    label: "message-delivery-label",
    path: "/message-delivery",
  },
  {
    key: "2",
    icon: <TemperatureIcon />,
    label: "temperature-management-label",
    path: "/temperature-management",
  },
  {
    key: "3",
    icon: <InjectionIcon />,
    label: "injection-management-label",
    path: "/injection-management",
  },
  {
    key: "4",
    icon: <UserIcon />,
    label: "user-management-label",
    path: "/user-list",
  },

  {
    key: "5",
    icon: <HouseIcon />,
    label: "class-management-label",
    path: "/class-management",
  },
  {
    key: "6",
    icon: <CircleIcon />,
    label: "active-management-label",
    path: "/active-management",
  },
  {
    key: "7",
    icon: <StudentAssociationIcon />,
    label: "change-student-association-label",
    path: "/change-student-association",
  },
  {
    key: "8",
    icon: <QRCodeIcon />,
    label: "qr-code-label",
    path: "/qr-code",
  },
  {
    key: "9",
    icon: <ContactIcon />,
    label: "account-label",
    path: "/account",
    children: [
      {
        key: "9.1",
        label: "account-list-label",
        path: "/account-list",
      },
      {
        key: "9.2",
        label: "add-account-label",
        path: "/add-account",
      },
    ],
  },
  {
    key: "10",
    icon: <SettingIcon />,
    label: "setting-label",
    path: "/setting",
  },
];

const MainLayout: React.FC = ({ children }) => {
  const { t }: any = useTranslation();
  const location = useLocation();
  const screens = useBreakpoint();
  const [isVisible, setIsVisible] = useState(false);
  const [isCollapsed, setIsCollapsed] = useState(false);
  const [dateTime, setDateTime] = useState(new Date());
  const widthSider = isCollapsed ? "80px" : "200px";
  const [openKeyItems, setOpenKeyItems] = useState(["1"]);
  const [openKeys, setOpenKeys] = React.useState(() => {
    if (
      location.pathname == "/account-list" ||
      location.pathname == "/add-account"
    ) {
      return ["sub1"];
    } else {
      return [];
    }
  });

  const handleToggle = () => {
    setIsCollapsed(!isCollapsed);
  };

  const showDrawer = () => {
    setIsVisible(!isVisible);
  };

  const handleOpenChange = (keys: any) => {
    setOpenKeys(keys);
  };

  const getDateTime = (date: any) => {
    const year = date.getFullYear();
    let month = date.getMonth() + 1;
    let day = date.getDate();
    month = month > 9 ? month : `0${month}`;
    day = day > 9 ? day : `0${day}`;
    
    const time = new Date().toLocaleTimeString("en-US", {
      hour12: false,
      hour: "numeric",
      minute: "numeric",
    });
    return `${year}年${month}月${day}日 ${time}`;
  };

  useEffect(() => {
    const timer = setInterval(() => {
      setDateTime(new Date());
    }, 1000);

    return () => {
      clearInterval(timer);
    };
  }, []);

  useEffect(() => {
    const item = MenuItems.find((_item: IItems) =>
      location.pathname.startsWith(_item.path)
    );
    const newOpenKeyItems = [];

    if (item) {
      newOpenKeyItems.push(item.key);
    }

    setOpenKeyItems(newOpenKeyItems);
  }, [location, MenuItems]);

  return (
    <Layout>
      {screens.xl && (
        <>
          <div
            style={{
              width: widthSider,
              overflow: "hidden",
              flex: `0 0 ${widthSider}`,
              maxWidth: widthSider,
              minWidth: widthSider,
              transition:
                "background-color 0.3s ease 0s, min-width 0.3s ease 0s, max-width 0.3s cubic-bezier(0.645, 0.045, 0.355, 1) 0s",
            }}
          ></div>
          <SiderStyled
            trigger={null}
            collapsible
            collapsed={isCollapsed}
            breakpoint="xl"
          >
            <Menu
              theme="light"
              mode="inline"
              openKeys={openKeys}
              selectedKeys={openKeyItems}
              onOpenChange={handleOpenChange}
            >
              {MenuItems.map((item) => {
                if (item.children?.length) {
                  return (
                    <SubMenu
                      key="sub1"
                      title={t(item.label)}
                      icon={<ContactIcon />}
                    >
                      {item.children?.map((subItem) => (
                        <Menu.Item key={subItem.key}>
                          <Link to={subItem.path}>{t(subItem.label)}</Link>
                        </Menu.Item>
                      ))}
                    </SubMenu>
                  );
                }
                return (
                  <Menu.Item key={item.key} icon={item.icon}>
                    <Link to={item.path}>{t(item.label)}</Link>
                  </Menu.Item>
                );
              })}
            </Menu>
          </SiderStyled>
        </>
      )}
      <Layout className="site-layout" style={{ minHeight: "100vh" }}>
        <div style={{ height: 60, background: "transparent" }}></div>
        <HeaderStyled className="site-layout-background">
          <WrapperHeaderWeb>
            <WrapperLogo>
              <Logo width="95px" height="16px" />
            </WrapperLogo>
            <CloseSidebarTextMobile>
              {t("school-management")}
            </CloseSidebarTextMobile>
            {isVisible ? (
              <CloseMenuIconStyled onClick={showDrawer} />
            ) : (
              <MenuIconStyled onClick={showDrawer} />
            )}

            <WrapperLeftHeader>
              <LineVertical />
              <FlexItem>
                {isCollapsed ? (
                  <OpenSidebarIcon onClick={handleToggle} />
                ) : (
                  <CloseSidebarIcon
                    onClick={handleToggle}
                    style={{ cursor: "pointer" }}
                  />
                )}
                <CloseSidebarText>{t("school-management")}</CloseSidebarText>
              </FlexItem>
              <FlexItem>
                <Time>{getDateTime(dateTime)}</Time>
                <Avatar>
                  <AvatarIcon />
                </Avatar>
              </FlexItem>
            </WrapperLeftHeader>
          </WrapperHeaderWeb>
        </HeaderStyled>
        <ContentStyled className="site-layout-background">
          {children}
        </ContentStyled>
        <FooterStyled>
          <CopyRight>© LEBER, Inc. All Rights Reserved.</CopyRight>
        </FooterStyled>
      </Layout>
      <Drawer
        title={
          <Avatar>
            <AvatarIcon />
          </Avatar>
        }
        onClose={showDrawer}
        visible={isVisible}
      >
        <Menu theme="light" mode="inline" selectedKeys={openKeyItems}>
          {MenuItems.map((item) => {
            if (item.children?.length) {
              return (
                <SubMenu
                  key="sub1"
                  title={t(item.label)}
                  icon={<ContactIcon />}
                >
                  {item.children?.map((subItem) => (
                    <Menu.Item key={subItem.key}>
                      <Link to={subItem.path}>{t(subItem.label)}</Link>
                    </Menu.Item>
                  ))}
                </SubMenu>
              );
            }
            return (
              <Menu.Item key={item.key} icon={item.icon}>
                <Link to={item.path}>{t(item.label)}</Link>
              </Menu.Item>
            );
          })}
        </Menu>
      </Drawer>
    </Layout>
  );
};

export default MainLayout;
