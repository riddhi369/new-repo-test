import React, { ReactElement } from "react";
import styled from "styled-components";

import { Logo } from "icons";
import { theme } from "theme/theme";

const Wrapper = styled.div`
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  padding-top: 80px;
  ${theme.media.tablet} {
    padding-top: 286px;
  }
  ${theme.media.laptopL} {
    padding-top: 100px;
  }
  ${theme.media.desktop} {
    padding-top: 240px;
  }
`;

const Container = styled.div`
  margin: 0 auto;
`;

const WrapperLogo = styled.div`
  margin-bottom: 80px;
  text-align: center;
  ${theme.media.tablet} {
    margin-bottom: 32px;
  }
`;

const WrapperContent = styled.div`
  position: relative;
  background: ${theme.colors.white};
  border-radius: 8px;
  box-shadow: 0px 0px 40px rgba(0, 0, 0, 0.05);
  width: 300px;
  padding: 32px 16px;
  margin-bottom: 192px;
  ${theme.media.mobileM} {
    width: 343px;
  }
  ${theme.media.tablet} {
    width: 450px;
    padding: 32px 40px;
    margin-bottom: 0px;
  }
`;

const WrapperCopyRight = styled.div`
  bottom: 0px;
  width: 100%;
  text-align: center;
  margin-bottom: 16px;
  ${theme.media.tablet} {
    position: absolute;
    margin-bottom: 32px;
  }
`;

const CopyRight = styled.div(({ theme }) => ({
  color: theme.colors.border,
  fontSize: theme.sizes.xs,
  fontWeight: theme.fontWeight.medium,
}));

const MainLayout: React.FC = ({ children }): ReactElement => {
  return (
    <Wrapper>
      <Container>
        <WrapperLogo>
          <Logo />
        </WrapperLogo>
        <WrapperContent>{children}</WrapperContent>
      </Container>
      <WrapperCopyRight>
        <CopyRight>© 2020 by LEBER, Inc. All Rights Reserved.</CopyRight>
      </WrapperCopyRight>
    </Wrapper>
  );
};

export default MainLayout;
