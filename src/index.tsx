import React from "react";
import ReactDOM from "react-dom";
import App from "./redux/rootRedux";
import "configs/i18n";

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);
