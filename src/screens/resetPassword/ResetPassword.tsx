import React, { useEffect } from "react";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";

import { Input, Button } from "components";
import {
  selectIsLoading,
  selectErrorMessage,
} from "redux/resetpassword/resetpasswordStates";
import { ResetPasswordData } from "models/resetPassword";
import { useDispatch, useSelector } from "react-redux";
import { ResetPasswordActionCreators } from "redux/rootActions";

interface IResetPasswordParams {
  password: string;
  confirmPassword: string;
  reset_password_token: string;
  role: number;
}

const schema = yup.object().shape({
  password: yup.string().trim().required("Password is required"),
  confirmPassword: yup
    .string()
    .required("Confirm password is required")
    .oneOf([yup.ref("password")], "Passwords must match"),
});

const ResetPassword: React.FC = () => {
  // const { push } = useHistory();
  const errorMessage = useSelector(selectErrorMessage);
  const isLoading = useSelector(selectIsLoading);
  const dispatch = useDispatch();
  const { watch, handleSubmit, control } = useForm({
    resolver: yupResolver(schema),
  });

  const watchPasswordField = watch("password");
  const watchConfirmPasswordField = watch("confirmPassword");

  useEffect(() => {
    dispatch(ResetPasswordActionCreators.handleErrorAction(""));
  }, [watchPasswordField, watchConfirmPasswordField]);

  const onSubmit = (data: IResetPasswordParams) => {
    // push("/reset-password-notification");
    /**
     * reset_password_token :Get this from URL sent to user email
     * role : Get this from URL sent to user email
     */
    const params: ResetPasswordData = {
      user: {
        password: data.password,
        password_confirmation: data.confirmPassword,
        reset_password_token: "Zhc9FXDxaFAKsXso7FbU",
        role: 1,
      },
    };
    dispatch(ResetPasswordActionCreators.resetPasswordUserAction(params));
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Controller
        control={control}
        name="password"
        render={({ field: { onChange }, fieldState: { error } }) => (
          <Input
            label="新しいパスワード"
            type="password"
            onChange={onChange}
            error={error?.message}
            marginForm="0 0 20px 0"
            fs={16}
            fwLabel={500}
          />
        )}
      />
      <Controller
        control={control}
        name="confirmPassword"
        render={({ field: { onChange }, fieldState: { error } }) => (
          <Input
            label="新しいパスワード（確認）"
            type="password"
            onChange={onChange}
            error={error?.message}
            marginForm="0 0 20px 0"
            fs={16}
            fwLabel={500}
          />
        )}
      />
      <div style={{ position: "relative" }}>
        {errorMessage && (
          <div
            style={{
              color: "#FB2121",
              fontSize: 12,
              fontWeight: 500,
              lineHeight: "12px",
              position: "absolute",
              top: -15,
            }}
          >
            {errorMessage}
          </div>
        )}
        <Button
          name="パスワードを変更"
          type="submit"
          background="#2F8CAE"
          color="#fff"
          bdr="6px"
          width="100%"
          margin="4px 0 0 0"
          padding="8px 0px"
          border="none"
          fontWeight={700}
          fontSize={16}
          disabled={isLoading}
        />
      </div>
    </form>
  );
};

export default ResetPassword;
