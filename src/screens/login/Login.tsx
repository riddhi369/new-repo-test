import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { useSelector, useDispatch } from "react-redux";
import { useTranslation } from "react-i18next";

import { Input, Button, Select } from "components";
import { LinkStyled } from "theme/CommonStyle";
import { ActionCreators } from "redux/rootActions";
import { LoginData } from "models/authentication";
import {
  selectIsLoading,
  selectErrorMessage,
} from "redux/authentication/authenticationStates";
import i18n from "configs/i18n";
import { localStorageHelper } from "utils";

const WrapperBottom = styled.div(({ theme }) => ({
  display: "flex",
  justifyContent: "space-between",
  alignItems: "center",
  a: {
    ...LinkStyled(theme),
  },
}));

const schema = yup.object().shape({
  email: yup.string().trim().required("Email is required"),
  password: yup.string().required("Password is required"),
});

const languageOptions = [
  {
    id: 1,
    value: "ja",
    name: "日本語",
  },
  {
    id: 2,
    value: "en",
    name: "English",
  },
];

const Login: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const currentLanguage = localStorageHelper.getItem("i18nextLng");
  const isLoading = useSelector(selectIsLoading);
  const errorMessage = useSelector(selectErrorMessage);
  const { watch, handleSubmit, control, setValue } = useForm({
    resolver: yupResolver(schema),
  });

  const watchEmailField = watch("email");
  const watchPasswordField = watch("password");
  const watchRoleField = watch("role");

  const options = [
    {
      id: 1,
      value: 1,
      name: t("administrator"),
    },
    {
      id: 2,
      value: 2,
      name: t("editor"),
    },
  ];

  useEffect(() => {
    dispatch(ActionCreators.handleErrorAction(""));
  }, [watchEmailField, watchPasswordField, watchRoleField]);

  useEffect(() => {
    setValue("role", options[0].value);
  }, []);

  const onSubmit = (data: any) => {
    const params: LoginData = {
      user: {
        ...data,
      },
    };
    dispatch(ActionCreators.loginUserAction(params));
  };

  const changeLanguage = (lng: string) => {
    i18n.changeLanguage(lng);
  };

  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Controller
          control={control}
          name="email"
          render={({ field: { onChange }, fieldState: { error } }) => (
            <Input
              label={t("email")}
              type="email"
              onChange={onChange}
              error={error?.message}
              marginForm="0 0 20px 0"
              fs={16}
              fwLabel={500}
            />
          )}
        />
        <Controller
          control={control}
          name="password"
          render={({ field: { onChange }, fieldState: { error } }) => (
            <Input
              label={t("password")}
              type="password"
              onChange={onChange}
              error={error?.message}
              marginForm="0 0 20px 0"
              fs={16}
              fwLabel={500}
            />
          )}
        />
        <Controller
          control={control}
          name="role"
          render={({ field: { onChange }, fieldState: { error } }) => (
            <Select
              defaultValue={1}
              label={t("role")}
              onChange={onChange}
              error={error?.message}
              marginForm="0 0 20px 0"
              options={options}
              height="40px"
              fs={16}
              fwLabel={500}
            />
          )}
        />
        <div style={{ position: "relative" }}>
          {errorMessage && (
            <div
              style={{
                color: "#FB2121",
                fontSize: 12,
                fontWeight: 500,
                lineHeight: "12px",
                position: "absolute",
                top: -15,
              }}
            >
              {errorMessage}
            </div>
          )}
          <Button
            name={t("login")}
            type="submit"
            background="#2F8CAE"
            color="#fff"
            bdr="6px"
            width="100%"
            margin="4px 0 24px 0"
            padding="8px 0px"
            border="none"
            fontWeight={700}
            fontSize={16}
            disabled={isLoading}
          />
        </div>
      </form>
      <WrapperBottom>
        <Select
          defaultValue={currentLanguage}
          options={languageOptions}
          height="40px"
          width="102px"
          fs={16}
          fwLabel={500}
          onChange={changeLanguage}
        />
        <Link to="/forgot-password">{t("forgot-password")}</Link>
      </WrapperBottom>
    </>
  );
};

export default Login;
