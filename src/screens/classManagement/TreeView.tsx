import React, { useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { Col, Row, Grid, Tooltip } from "antd";
import { useTranslation } from "react-i18next";

import { Input, Button } from "components";
import { MoreIcon, PlusIcon } from "icons";
import { uuid } from "utils";

import EditDeleteDropdown from "./EditDeleteDropdown";
import DeleteModal from "./DeleteModal";
import {
  AddButton,
  WrapperItem,
  ItemName,
  DropdownIconStyled,
  ItemWrapper,
  VerticalTilesIconStyled,
  BtnGroup,
  PlusCircleIconStyled,
  SaveBtnStyled,
  AddText,
  InputStyled,
  Form,
  WrapperTreeView,
} from "./ClassManagement.style";
import { ITreeData } from "./ClassManagement";

const { useBreakpoint } = Grid;

interface ITreeView {
  treeData: ITreeData[];
  setTreeData: React.Dispatch<React.SetStateAction<ITreeData[]>>;
}

const TreeView: React.FC<ITreeView> = ({ treeData, setTreeData }) => {
  const { t }: any = useTranslation();
  const screens = useBreakpoint();

  const [displayAdds, setDisplayAdds] = useState<any>({});
  const [displayEditDeletes, setDisplayEditDeletes] = useState<any>({});
  const [idEdit, setIdEdit] = useState("");
  const [idDelete, setIdDelete] = useState("");
  const [addedItemValue, setAddedItemValue] = useState("");

  const [isVisibleDeleteModal, setIsVisibleDeleteModal] = useState(false);
  const [isChildren, setIsChildren] = useState(false);
  const [isShowItemAddition, setIsShowItemAddition] = useState(false);
  const { handleSubmit, control, setValue, reset } = useForm({
    defaultValues: {
      name: "",
    },
  });

  const handleChangeInputAddItem = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setAddedItemValue(event.target.value);
  };

  const handleAddItem = () => {
    const item = {
      name: addedItemValue,
      id: uuid(),
      dataChildren: [],
    };
    setTreeData([...treeData, item]);
    setAddedItemValue("");
    setIsShowItemAddition(!isShowItemAddition);
  };

  const handleShowMenuDropdown = (id: string, value = true) => {
    setDisplayEditDeletes({ [id]: value });
  };
  const handleAdd = (id: string, value = true) => {
    setDisplayAdds({ [id]: value });
    setValue("name", "");
    setIdEdit("");
  };

  const handleSave = (item: ITreeData) => (data: { name: string }) => {
    if (idEdit == "") {
      item.dataChildren.push({
        name: data.name,
        id: uuid(),
        dataChildren: [],
      });
    } else {
      item.name = data.name;
    }
    reset();
    setTreeData([...treeData]);
    setIdEdit("");
    setDisplayAdds({});
  };

  const handleEdit = (props: ITreeData) => {
    handleShowMenuDropdown(props.id, false);
    setDisplayAdds({});
    setValue("name", props.name);
    setIdEdit(props.id);
  };

  const handleClose = (props: ITreeData) => {
    setDisplayAdds({});
    handleShowMenuDropdown(props.id, false);
  };

  const handleConfirmleDelete = (props: ITreeData) => {
    if (props.dataChildren.length) {
      setIsChildren(true);
    } else {
      setIsChildren(false);
    }
    setIsVisibleDeleteModal(true);
    setIdDelete(props.id);
  };

  const deleteDatas = (items: ITreeData[], id: string) => {
    items.forEach((item, index) => {
      if (item.id == id) {
        items.splice(index, 1);
        return;
      }
      if (item.dataChildren.length) {
        deleteDatas(item.dataChildren, id);
      }
    });
  };

  const handleDelete = () => {
    deleteDatas(treeData, idDelete);
    setTreeData([...treeData]);
    setIdDelete("");
    setIsVisibleDeleteModal(false);
  };

  const handleCancel = () => {
    setIsVisibleDeleteModal(false);
    setDisplayEditDeletes({});
  };

  const CompInputAdd = ({
    item,
    type = "add",
  }: {
    item: ITreeData;
    type?: string;
  }) => {
    return (
      <Form
        style={{ margin: "8px 0px", display: "flex" }}
        onSubmit={handleSubmit(handleSave(item))}
      >
        <Controller
          control={control}
          name="name"
          render={({ field: { onChange, value }, fieldState: { error } }) => (
            <InputStyled
              value={value}
              error={error?.message}
              height={31}
              fs={16}
              fwLabel={500}
              marginForm="0px 8px 0px 0px"
              onChange={(e: any) => {
                onChange(e.target.value);
              }}
            />
          )}
        />
        <SaveBtnStyled
          type="submit"
          background="#2AC769"
          color="#fff"
          name={type !== "add" ? t("keep") : t("create")}
          border="none"
          bdr="6px"
          fontSize={16}
          fontWeight={700}
        />
      </Form>
    );
  };

  const CompItem = ({ item, children }: any) => {
    const { name, id, dataChildren } = item;
    if (id == idEdit) {
      return (
        <>
          <CompInputAdd item={item} type="edit" />
          {children}
        </>
      );
    }

    return (
      <>
        <ItemWrapper>
          <VerticalTilesIconStyled fill="#FBFBFB" />
          {!!dataChildren.length && <DropdownIconStyled fill="#B7B7B7" />}

          <Tooltip placement="topLeft" title={name}>
            <ItemName style={dataChildren.length ? { marginLeft: 20 } : {}}>
              {name}
            </ItemName>
          </Tooltip>
          <BtnGroup>
            <div style={{ position: "relative", height: 24 }}>
              <MoreIcon
                fill="#B7B7B7"
                onClick={() => {
                  handleShowMenuDropdown(id, true);
                }}
              />
              {displayEditDeletes[id] && (
                <EditDeleteDropdown
                  item={item}
                  handleEdit={handleEdit}
                  handleConfirmleDelete={handleConfirmleDelete}
                  handleClose={handleClose}
                />
              )}
            </div>
            <PlusCircleIconStyled
              fill="#2AC769"
              onClick={() => {
                handleAdd(id);
              }}
            />
          </BtnGroup>
        </ItemWrapper>
        {children}
      </>
    );
  };

  const Item = ({ item }: { item: ITreeData }) => {
    const { dataChildren, id } = item;
    return (
      <CompItem item={item}>
        {dataChildren.length > 0 &&
          dataChildren.map((i: ITreeData) => (
            <div style={{ marginLeft: 32 }} key={i.id}>
              <CompItem item={i}>
                {i.dataChildren.map((a: ITreeData) => (
                  <div style={{ marginLeft: 32 }} key={a.id}>
                    <Item item={a} />
                  </div>
                ))}
                {displayAdds[i.id] && <CompInputAdd item={i} />}
              </CompItem>
            </div>
          ))}
        {displayAdds[id] && <CompInputAdd item={item} />}
      </CompItem>
    );
  };

  const renderCreateButton = () => (
    <AddButton>
      <AddText
        onClick={() => {
          setIsShowItemAddition(true);
        }}
      >
        <PlusIcon fill="#1AB759" />
        <span style={{ marginLeft: 5, color: "#1ab759" }}>{t("add-item")}</span>
      </AddText>
      {isShowItemAddition && (
        <div style={{ display: "flex" }}>
          <Input
            bdr={8}
            height={31}
            marginForm="0px 0px 0px 10px"
            value={addedItemValue}
            onChange={handleChangeInputAddItem}
          />
          <Button
            onClick={handleAddItem}
            background="#2AC769"
            color="#fff"
            margin="0 0 0 8px"
            name={t("create")}
            border="none"
            bdr="6px"
            fontSize={16}
            fontWeight={700}
          />
        </div>
      )}
    </AddButton>
  );

  return (
    <>
      <div style={{ marginBottom: 32 }}>{renderCreateButton()}</div>
      <WrapperTreeView>
        {(treeData as ITreeData[]).map((item) => (
          <WrapperItem key={item.id}>
            <Item item={item} />
          </WrapperItem>
        ))}
      </WrapperTreeView>
      <div style={{ marginBottom: 8, marginTop: 32 }}>
        {renderCreateButton()}
      </div>
      {isVisibleDeleteModal && (
        <DeleteModal
          isVisibleDeleteModal={isVisibleDeleteModal}
          isChildren={isChildren}
          handleCancel={handleCancel}
          handleDelete={handleDelete}
        />
      )}
    </>
  );
};

export default TreeView;
