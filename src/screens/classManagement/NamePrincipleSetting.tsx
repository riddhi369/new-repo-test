import React from "react";
import { Controller, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { Grid } from "antd";
import { useTranslation } from "react-i18next";

import { Input, Select } from "components";

import { ButtonStyled, WrapperSettings } from "./ClassManagement.style";
import { ISubmitedData } from "./ClassManagement";

const { useBreakpoint } = Grid;

const schoolOptions = [
  {
    key: 1,
    value: "elementary",
    name: "小学校（6年）",
  },
  {
    key: 2,
    value: "junior",
    name: "中学校（3年）",
  },
  {
    key: 3,
    value: "highSchool",
    name: "高校（3年）",
  },
  {
    key: 4,
    value: "university",
    name: "大学（4年）",
  },
];

const classNamePrincipleOptions = [
  {
    key: 1,
    value: "number",
    name: "数字",
  },
  {
    key: 2,
    value: "alphabet",
    name: "アルファベット",
  },
];

const ClassNumberIsValid = "class-number-invalid";
const schema = yup.object().shape({
  schoolClassification: yup.string().required("select-school-classification"),
  namePrinciple: yup.string().required("select-name-principle"),
  numberOfClasses: yup
    .number()
    .positive(ClassNumberIsValid)
    .integer(ClassNumberIsValid)
    .max(26, ClassNumberIsValid)
    .required("enter-class-number"),
});

interface INamePrincipe {
  handlePrincipleSetting: (data: ISubmitedData) => void;
}

const NamePrincipleSetting: React.FC<INamePrincipe> = ({
  handlePrincipleSetting,
}) => {
  const { t }: any = useTranslation();
  const screens = useBreakpoint();
  const { handleSubmit, control } = useForm({
    resolver: yupResolver(schema),
  });

  const labelInput = screens.xl ? (
    <div
      style={{
        position: "absolute",
        top: -13,
        width: 57,
      }}
    >
      <div style={{ width: 103 }}>{t("each-grade")}</div>
      <div style={{ width: 130 }}>{t("class-number")}</div>
    </div>
  ) : (
    <>{t("class-number")}</>
  );
  return (
    <WrapperSettings onSubmit={handleSubmit(handlePrincipleSetting)}>
      <Controller
        control={control}
        name="schoolClassification"
        render={({ field: { onChange }, fieldState: { error } }) => (
          <Select
            label={t("school-classification")}
            placeholder="選択してください"
            width="156px"
            marginForm="0 32px 16px 0"
            fsLabel={14}
            fwLabel={500}
            lhLabel="20px"
            options={schoolOptions}
            onChange={onChange}
            error={t(error?.message)}
          />
        )}
      />
      <Controller
        control={control}
        name="namePrinciple"
        render={({ field: { onChange }, fieldState: { error } }) => (
          <Select
            label={t("name-principle")}
            placeholder="選択してください"
            width="156px"
            marginForm="0 32px 16px 0"
            fsLabel={14}
            fwLabel={500}
            lhLabel="20px"
            options={classNamePrincipleOptions}
            onChange={onChange}
            error={t(error?.message)}
          />
        )}
      />
      <Controller
        control={control}
        name="numberOfClasses"
        render={({ field: { onChange }, fieldState: { error } }) => (
          <Input
            type="number"
            marginForm="0 32px 16px 0"
            label={labelInput}
            height={31}
            width={48}
            fsLabel={14}
            fwLabel={500}
            lhLabel="16px"
            onChange={onChange}
            error={t(error?.message)}
          />
        )}
      />
      <ButtonStyled
        type="submit"
        name={t("create")}
        background="#2AC769"
        color="#FFFFFF"
        padding="3px 16px"
        border="none"
        bdr="6px"
        fontSize={16}
        fontWeight={700}
        lineHeight="16px"
      />
    </WrapperSettings>
  );
};

export default NamePrincipleSetting;
