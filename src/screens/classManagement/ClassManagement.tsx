import React, { useState } from "react";
import { Grid } from "antd";
import { useTranslation } from "react-i18next";

import { Box } from "components";
import { uuid } from "utils";

import NamePrincipleSetting from "./NamePrincipleSetting";
import TreeView from "./TreeView";
import { Wrapper } from "./ClassManagement.style";

const { useBreakpoint } = Grid;

interface IChildrenData {
  name: string;
  id: string;
  dataChildren: any;
}
export interface ITreeData {
  name: string;
  id: string;
  dataChildren: IChildrenData[];
}

export interface ISubmitedData {
  schoolClassification: string;
  namePrinciple: string;
  numberOfClasses: number;
}

const NumberOfSchoolYears: any = {
  university: 4,
  elementary: 6,
  junior: 3,
  highSchool: 3,
};

const ALPHABET = [
  "A",
  "B",
  "C",
  "D",
  "E",
  "F",
  "G",
  "H",
  "I",
  "J",
  "K",
  "L",
  "M",
  "N",
  "O",
  "P",
  "Q",
  "R",
  "S",
  "T",
  "U",
  "V",
  "W",
  "X",
  "Y",
  "Z",
];

const ClassManagement: React.FC = () => {
  const { t }: any = useTranslation();
  const [treeData, setTreeData] = useState<ITreeData[]>([]);

  const handleCheckNamePrinciple = (namePrinciple: string, j: number) =>
    namePrinciple === "number" ? `${j + 1}組` : `${ALPHABET[j]}組`;

  const handlePrincipleSetting = (data: ISubmitedData) => {
    const newData: ITreeData[] = [];
    for (let i = 0; i < NumberOfSchoolYears[data.schoolClassification]; i++) {
      newData.push({
        name: `${i + 1}年`,
        id: uuid(),
        dataChildren: [],
      });
      for (let j = 0; j < data.numberOfClasses; j++) {
        newData[i].dataChildren.push({
          name: handleCheckNamePrinciple(data.namePrinciple, j),
          id: uuid(),
          dataChildren: [],
        });
      }
    }

    setTreeData([...newData]);
  };

  return (
    <Box title={t("class-management-label")}>
      <Wrapper>
        {!treeData.length ? (
          <NamePrincipleSetting
            handlePrincipleSetting={handlePrincipleSetting}
          />
        ) : (
          <TreeView treeData={treeData} setTreeData={setTreeData} />
        )}
      </Wrapper>
    </Box>
  );
};

export default ClassManagement;
