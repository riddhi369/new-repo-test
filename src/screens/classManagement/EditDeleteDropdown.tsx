import React from "react";
import { useTranslation } from "react-i18next";

import { CloseIcon, EditIcon, TrashIcon } from "icons";
import {
  WrapperDropdown,
  WrapperCloseIcon,
  Text,
} from "./ClassManagement.style";
import { ITreeData } from "./ClassManagement";

interface IEditDeleteDropDown {
  item: ITreeData;
  handleEdit: (props: ITreeData) => void;
  handleConfirmleDelete: (props: ITreeData) => void;
  handleClose: (props: ITreeData) => void;
}
const EditDeleteDropdown: React.FC<IEditDeleteDropDown> = ({
  item,
  handleEdit,
  handleConfirmleDelete,
  handleClose,
}) => {
  const { t }: any = useTranslation();

  return (
    <WrapperDropdown>
      <div
        style={{ display: "flex", marginBottom: 11, cursor: "pointer" }}
        onClick={() => {
          handleEdit(item);
        }}
      >
        <EditIcon fill="#2F8CAE" />
        <Text style={{ color: "#2F8CAE" }}>{t("edit")}</Text>
      </div>

      <div
        style={{ display: "flex", cursor: "pointer" }}
        onClick={() => {
          handleConfirmleDelete(item);
        }}
      >
        <TrashIcon fill="#FB4E4E" />
        <Text style={{ color: "#FB4E4E" }}>{t("delete")}</Text>
      </div>

      <WrapperCloseIcon
        onClick={() => {
          handleClose(item);
        }}
      >
        <CloseIcon fill="#2F8CAE" height="14px" width="14px" />
      </WrapperCloseIcon>
    </WrapperDropdown>
  );
};

export default EditDeleteDropdown;
