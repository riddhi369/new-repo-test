import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";

import { LinkStyled } from "theme/CommonStyle";

const Message = styled.p(({ theme }) => ({
  fontSize: theme.sizes.sm,
  fontWeight: theme.fontWeight.medium,
  color: theme.colors.text.primary,
  lineHeight: "20px",
  marginBottom: 24,
}));

const WrapperLink = styled.div(({ theme }) => ({
  textAlign: "center",
  a: {
    ...LinkStyled(theme),
  },
}));

const ResetPasswordNotification: React.FC = () => {
  return (
    <>
      <Message>
        パスワードを再設定しました。ログイン画面に移り、再度ログインをお試しください。
      </Message>
      <WrapperLink>
        <Link to="/">ログインへ</Link>
      </WrapperLink>
    </>
  );
};

export default ResetPasswordNotification;
