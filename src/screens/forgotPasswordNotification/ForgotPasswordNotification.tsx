import React, { useEffect } from "react";
import styled from "styled-components";
import { useHistory } from "react-router-dom";

import { LinkStyled } from "theme/CommonStyle";

const Message = styled.p(({ theme }) => ({
  fontSize: theme.sizes.sm,
  fontWeight: theme.fontWeight.medium,
  color: theme.colors.text.primary,
  lineHeight: "20px",
  marginBottom: 24,
}));

const WrapperLink = styled.div(({ theme }) => ({
  textAlign: "center",
  div: {
    ...LinkStyled(theme),
  },
}));

const ForgotPasswordNotification: React.FC = () => {
  const { push } = useHistory();

  const handleResendEmail = () => {
    if (window.confirm('Click on "OK" button to reset password')) {
      push("reset-password");
    }
  };

  useEffect(() => {
    if (window.confirm('Click on "OK" button to reset password')) {
      push("reset-password");
    }
  }, []);
  return (
    <>
      <Message>
        入力されたメールアドレスに送信されたメールを確認し、パスワードの再設定を行ってください。
      </Message>
      <WrapperLink>
        <div onClick={handleResendEmail}>メールが届いていない方はこちら</div>
      </WrapperLink>
    </>
  );
};

export default ForgotPasswordNotification;
