import { Row, Col, TreeSelect } from "antd";
import React, { useEffect } from "react";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { useTranslation } from "react-i18next";

const { TreeNode } = TreeSelect;

import {
  Input as Input,
  Select,
  Modal,
  DatePicker,
  TreeSelectCustom,
} from "components";
import { IModal } from "./UserList.modal";
import { localStorageHelper } from "utils";
import { SubDepartmentListData } from "models/subdepartmentList";
import { subDepartmentListDataResponse } from "redux/subdepartment/subdepartmentListStates";
import {
  CreateUserActionCreators,
  SubDepartmentListActionCreators,
} from "redux/rootActions";
import { useDispatch, useSelector } from "react-redux";
import { Label } from "./UserList.style";
import { IGroupListOption } from "models/activities";
import { CreateUserData } from "models/createuserlist";
import {
  createUserSuccessResponse,
  selectCreateUserErrorMessage,
} from "redux/createuserlist/createuserStates";
import { ImportantStyled } from "./UserList.style";

const schema = yup.object().shape({
  lastName: yup.string().trim().required("last-name-required"),
  firstName: yup.string().trim().required("first-name-required"),
  gender: yup.string().required("gender-required"),
  schoolYear: yup.string().required("school-year-required"),
  class: yup.string().required("class-required").nullable(),
});

const CreatingModal = ({
  visible,
  handleCancel,
  handleSubmitOK,
  FooterModal,
  activityGroupOptions,
  departmentOptions,
  genderOptions,
}: IModal) => {
  const { t }: any = useTranslation();
  const { watch, handleSubmit, control, reset } = useForm({
    reValidateMode: "onChange",
    defaultValues: {
      lastName: "",
      firstName: "",
      gender: undefined,
      birthDay: new Date(),
      attendance: "",
      schoolYear: undefined,
      class: undefined,
      activities: undefined,
    },
    resolver: yupResolver(schema),
  });
  const currentLanguage = localStorageHelper.getItem("i18nextLng");
  const dispatch = useDispatch();
  const subdepartmentOptions = useSelector(subDepartmentListDataResponse);
  const createUserSuccess = useSelector(createUserSuccessResponse);
  const createUserErrorMessage = useSelector(selectCreateUserErrorMessage);

  const [activity, setActivities] = React.useState<IGroupListOption[]>();
  const watchSchoolYearField = watch("schoolYear");

  useEffect(() => {
    setActivities(
      activityGroupOptions && activityGroupOptions?.group_list !== undefined
        ? activityGroupOptions?.group_list
        : []
    );
  }, []);

  useEffect(() => {
    if (watchSchoolYearField !== undefined) {
      const subDepartmentparams: SubDepartmentListData = {
        locale: currentLanguage,
        department_id: watchSchoolYearField,
        role: 1,
      };
      dispatch(
        SubDepartmentListActionCreators.subDepartmentListAction(
          subDepartmentparams
        )
      );
    }
  }, [watchSchoolYearField]);

  const resetForm = (event?: any) => {
    if (event !== undefined) event.target.reset();
    reset();
  };

  const onSubmit = (data: any, e: any) => {
    e.preventDefault();
    const createUserParams: CreateUserData = {
      company_user: {
        first_name: data.firstName,
        last_name: data.lastName,
        furi_first_name: "ウジュバル",
        furi_last_name: "シリマリ",
        gender: data.gender === "男性" ? 0 : 1,
        date_of_birth: data.birthDay,
        dept_id: data.schoolYear,
        sub_dept_id: data.class,
        roll_number: data.attendance,
        company_sub_department_id: data.activities,
      },
      locale: currentLanguage,
    };
    dispatch(CreateUserActionCreators.createUserAction(createUserParams));
    if (createUserSuccess?.message) {
      window.confirm(createUserSuccess.message);
      resetForm();
      handleCancel();
    } else {
      window.confirm(createUserErrorMessage);
    }
  };

  return (
    <Modal
      type="form"
      title={t("user-registration")}
      visible={visible}
      onCancel={handleCancel}
      onOk={handleSubmit(onSubmit)}
    >
      <form onSubmit={handleSubmit(onSubmit)}>
        <Row gutter={16}>
          <Col xs={24} md={12} xl={12}>
            <Controller
              control={control}
              name="lastName"
              render={({
                field: { onChange, value },
                fieldState: { error },
              }) => (
                <Input
                  value={value}
                  label={
                    <>
                      {t("last-name")}
                      <ImportantStyled>*</ImportantStyled>
                    </>
                  }
                  height={31}
                  fsLabel={16}
                  marginForm="0 0 22px 0"
                  onChange={onChange}
                  error={t(error?.message)}
                />
              )}
            />
          </Col>
          <Col xs={24} md={12} xl={12}>
            <Controller
              control={control}
              name="firstName"
              render={({
                field: { onChange, value },
                fieldState: { error },
              }) => (
                <Input
                  value={value}
                  label={
                    <>
                      {t("first-name")}
                      <ImportantStyled>*</ImportantStyled>
                    </>
                  }
                  height={31}
                  fsLabel={16}
                  marginForm="0 0 22px 0"
                  onChange={onChange}
                  error={t(error?.message)}
                />
              )}
            />
          </Col>
        </Row>

        <Row gutter={16}>
          <Col xs={12} md={8} xl={8}>
            <Controller
              control={control}
              name="gender"
              render={({
                field: { onChange, value },
                fieldState: { error },
              }) => (
                <Select
                  value={value}
                  label={t("gender")}
                  placeholder={t("please-select")}
                  options={genderOptions}
                  fsLabel={16}
                  marginForm="0 0 22px 0"
                  onChange={onChange}
                  error={t(error?.message)}
                />
              )}
            />
          </Col>
          <Col xs={12} md={8} xl={8}>
            <Controller
              control={control}
              name="birthDay"
              render={({
                field: { onChange, value },
                fieldState: { error },
              }) => (
                <DatePicker
                  selected={value}
                  onChange={onChange}
                  error={t(error?.message)}
                  dateFormat={"yyyy/MM/dd"}
                  label={t("birthday")}
                />
              )}
            />
          </Col>
          <Col xs={12} md={8} xl={8}>
            <Controller
              control={control}
              name="attendance"
              render={({
                field: { onChange, value },
                fieldState: { error },
              }) => (
                <Input
                  value={value}
                  label={t("attendance")}
                  height={31}
                  fsLabel={16}
                  marginForm="0 0 22px 0"
                  onChange={onChange}
                  error={t(error?.message)}
                />
              )}
            />
          </Col>
        </Row>

        <Row gutter={16}>
          <Col xs={24} md={12} xl={12}>
            <Controller
              control={control}
              name="schoolYear"
              render={({
                field: { onChange, value },
                fieldState: { error },
              }) => (
                <Select
                  value={value}
                  label={
                    <>
                      {t("school-year")}
                      <ImportantStyled>*</ImportantStyled>
                    </>
                  }
                  placeholder={t("please-select")}
                  options={departmentOptions}
                  fsLabel={16}
                  marginForm="0 0 22px 0"
                  onChange={onChange}
                  error={t(error?.message)}
                />
              )}
            />
          </Col>
          <Col xs={24} md={12} xl={12}>
            <Controller
              control={control}
              name="class"
              render={({
                field: { onChange, value },
                fieldState: { error },
              }) => (
                <Select
                  value={value}
                  label={
                    <>
                      {t("class")}
                      <ImportantStyled>*</ImportantStyled>
                    </>
                  }
                  placeholder={t("please-select")}
                  options={subdepartmentOptions?.result}
                  fsLabel={16}
                  disabled={watchSchoolYearField !== undefined ? false : true}
                  marginForm="0 0 22px 0"
                  onChange={onChange}
                  error={t(error?.message)}
                />
              )}
            />
          </Col>
        </Row>
        <Label>{t("activities")}</Label>
        {activityGroupOptions?.compulsory_group !== undefined ? (
          <Label>
            Compulsory Groups : {activityGroupOptions?.compulsory_group}
          </Label>
        ) : (
          ""
        )}
        <Controller
          control={control}
          name="activities"
          render={({ field: { onChange, value }, fieldState: { error } }) => (
            <TreeSelectCustom
              error={t(error?.message)}
              placeholder={t("please-select-activities")}
              value={value}
              onChange={onChange}
            >
              {activity &&
                activity.length > 0 &&
                activity.map((it: IGroupListOption) => (
                  <>
                    <TreeNode
                      value={it.id}
                      title={it.text}
                      disabled={
                        it.children && it.children.length > 0 ? true : false
                      }
                    >
                      {it.children &&
                        it.children.length > 0 &&
                        it.children.map((child: IGroupListOption) => (
                          <>
                            <TreeNode
                              value={child.id}
                              title={child.text}
                            ></TreeNode>
                          </>
                        ))}
                    </TreeNode>
                  </>
                ))}
            </TreeSelectCustom>
          )}
        />
        {FooterModal(
          "#FFFFFF",
          "#E0E0E0",
          "#2AC769",
          "11px 24px 11px",
          t("cancel"),
          t("register"),
          handleCancel,
          handleSubmitOK
        )}
      </form>
    </Modal>
  );
};

export default CreatingModal;
