import React, { useState } from "react";
import { Grid } from "antd";
import { useTranslation } from "react-i18next";

import { Modal, DateRangePicker, Button } from "components";
import { DownloadIcon } from "icons";
import { AttendanceLabel, DataRow, WrapperHeaderBtn } from "./UserList.style";
import { IModal } from "./UserList.modal";

const { useBreakpoint } = Grid;

const AttendanceModal = ({ visible, handleCancel }: IModal) => {
  const screens = useBreakpoint();
  const { t } = useTranslation();

  const [startDate, setStartDate] = useState(new Date("2014/02/08"));
  const [endDate, setEndDate] = useState(new Date("2014/02/10"));

  const HeaderModal = () => {
    return (
      <div>
        <DateRangePicker
          startDate={startDate}
          endDate={endDate}
          setStartDate={setStartDate}
          setEndDate={setEndDate}
        />
        <WrapperHeaderBtn>
          <Button
            icon={
              <DownloadIcon
                width="14px"
                height="14px"
                fill="currentColor"
                style={{ position: "absolute", left: 4, top: 5 }}
              />
            }
            name={t("pdf-output")}
            background="#2AC769"
            color="#FFFFFF"
            border="none"
            fontSize={12}
            fontWeight={700}
            padding="1px 8px 1px 17px"
          />
        </WrapperHeaderBtn>
      </div>
    );
  };

  return (
    <Modal
      type="search"
      title={screens.md ? <HeaderModal /> : <div></div>}
      visible={visible}
      onCancel={handleCancel}
    >
      <div>
        {!screens.md && <HeaderModal />}
        <AttendanceLabel>
          リーバー太郎さんの2021/10/01〜2021/10/31までの出欠表
        </AttendanceLabel>

        {[...Array(20)].map((_, key) => {
          return (
            <DataRow key={key}>
              <span>2021/10/01（金）</span>
              <p>出席</p>
            </DataRow>
          );
        })}
        <DataRow>
          <span>2021/10/01（金）</span>
          <p>欠席（病欠）「微熱が出たので、念のために休ませます。」</p>
        </DataRow>
      </div>
    </Modal>
  );
};

export default AttendanceModal;
