import React, { useEffect, useState } from "react";
import { Grid, Dropdown, Menu } from "antd";
import { useTranslation } from "react-i18next";
import { useForm, Controller } from "react-hook-form";

import { Input as Input, Button, Box, Pagination, Select } from "components";
import { PlusIcon, DownloadIcon, EditIcon, MoreIcon } from "icons";

import {
  TaskWrapper,
  TaskWrapperLeft,
  TaskWrapperRight,
  PaginationWrapper,
  ContentWrapper,
  Text,
  TextLink,
  SearchWrapper,
  SearchWrapperLeft,
  WrapperSelectAndBtn,
  WrapperBtnForm,
  RowCellWrapper,
  TableStyled,
  BadgeContainer,
} from "./UserList.style";
import UpdatingModal from "./UpdatingModal";
import CreatingModal from "./CreatingModal";
import AttendanceModal from "./AttendanceModal";
import BatchModal from "./BatchModal";
import {
  UserListActionCreators,
  ClassListActionCreators,
  DepartmentListActionCreators,
  ActivitiesListActionCreators,
} from "redux/rootActions";
import { useDispatch, useSelector } from "react-redux";
import {
  userListDataResponse,
  selectErrorMessage,
} from "redux/userlist/userListStates";
import { UserListData, UserListResultData } from "models/userlist";
import { localStorageHelper } from "utils";
import { ClassListData } from "models/classList";
import { classListDataResponse } from "redux/classlist/classListStates";
import { departmentListDataResponse } from "redux/department/departmentListStates";
import { DepartmentListData } from "models/departmentList";
import { ActivitiesListData } from "models/activities";
import { activitiesListDataResponse } from "redux/activitieslist/activitiesListStates";

type FormStatus = "creating" | "updating" | "batch" | "attendance" | "";

const PAGE_SIZE = 25;
const { useBreakpoint } = Grid;

const UserList: React.FC = () => {
  const screens = useBreakpoint();
  const { t } = useTranslation();
  const [visible, setVisible] = useState(false);
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [formStatus, setFormStatus] = useState<FormStatus>("");
  const [selectedItem, setSelectedItem] = useState<UserListResultData>();

  const { watch, handleSubmit, control, setValue } = useForm();
  const dispatch = useDispatch();
  const userlistdata = useSelector(userListDataResponse);
  const classlistdata = useSelector(classListDataResponse);
  const errorMessageUserList = useSelector(selectErrorMessage);
  const departmentlistdata = useSelector(departmentListDataResponse);
  const activitieslistdata = useSelector(activitiesListDataResponse);

  const watchClassField = watch("grade");
  const watchSearchNameField = watch("name");
  const watchNumberRecordPerPage = watch("number_record");

  const registerBtnName = screens.md ? t("user-registration") : t("register");
  const batchRegisterBtnName = screens.md
    ? t("user-batch-registration")
    : t("batch-registration");
  const currentLanguage = localStorageHelper.getItem("i18nextLng");

  useEffect(() => {
    setValue("grade", "");
    setValue("number_record", "25");
  }, []);

  useEffect(() => {
    const classListparams: ClassListData = {
      locale: currentLanguage,
    };
    dispatch(ClassListActionCreators.classListAction(classListparams));

    const departmentparams: DepartmentListData = {
      locale: currentLanguage,
      role: 1,
    };
    dispatch(
      DepartmentListActionCreators.departmentListAction(departmentparams)
    );

    const activitiesParams: ActivitiesListData = {
      locale: currentLanguage,
    };
    dispatch(
      ActivitiesListActionCreators.activitiesListAction(activitiesParams)
    );
  }, []);

  const getUserApiCall = () => {
    const paramsUserList: UserListData = {
      page: currentPage,
      c_department_id: "",
      user_name: "",
      locale: currentLanguage,
      per_page:
        watchNumberRecordPerPage !== undefined
          ? watchNumberRecordPerPage
          : PAGE_SIZE,
    };
    dispatch(UserListActionCreators.userListAction(paramsUserList));
  };

  useEffect(() => {
    getUserApiCall();
  }, [currentPage, watchNumberRecordPerPage]);

  const genderOptions = [
    {
      id: 0,
      name: "男性",
      value: "男性",
    },
    {
      id: 1,
      name: "女性",
      value: "女性",
    },
  ];
  const rightOptions = [
    {
      id: 1,
      name: t("25-cases"),
      value: "25",
    },
    {
      id: 2,
      name: t("50-cases"),
      value: "50",
    },
    {
      id: 3,
      name: t("100-cases"),
      value: "100",
    },
  ];

  const menu = (
    <Menu>
      <Menu.Item>
        <Button
          icon={
            <DownloadIcon
              width="14px"
              height="14px"
              fill="currentColor"
              style={{ position: "absolute", left: 4, top: 6 }}
            />
          }
          name={t("excel-output")}
          background="#2AC769"
          color="#FFFFFF"
          border="none"
          fontSize={12}
          fontWeight={700}
          padding="3px 7px 3px 16px"
        />
      </Menu.Item>
      <Menu.Item>
        <Button
          icon={
            <DownloadIcon
              width="14px"
              height="14px"
              fill="currentColor"
              style={{ position: "absolute", left: 4, top: 6 }}
            />
          }
          name={t("excel-output")}
          background="#2AC769"
          color="#FFFFFF"
          border="none"
          fontSize={12}
          fontWeight={700}
          padding="3px 7px 3px 16px"
        />
      </Menu.Item>
    </Menu>
  );

  const showModal = (formStatus: FormStatus) => {
    setFormStatus(formStatus);
    setVisible(true);
  };

  const columns = [
    {
      title: t("full-name"),
      dataIndex: ["last_name", "first_name"],
      key: "first_name",
      width: 175,
      fixed: "left",
      render: (name: string, row: any) => (
        <div style={{ display: "flex" }}>
          <div style={{ marginTop: 3 }}>
            <EditIcon
              onClick={() => showModal("updating")}
              fill="currentColor"
              style={{ cursor: "pointer" }}
              width="16px"
              height="16px"
            />
          </div>
          <TextLink onClick={() => showModal("attendance")}>
            {row["last_name"] + " " + row["first_name"]}
          </TextLink>
        </div>
      ),
    },
    {
      title: t("other-name-type"),
      dataIndex: ["furi_first_name", "furi_last_name"],
      key: "furi_first_name",
      width: 155,
      sorter: {},
      render: (name: string, row: any) => (
        <div style={{ display: "flex" }}>
          <Text>{row["furi_first_name"] + " " + row["furi_last_name"]}</Text>
        </div>
      ),
    },
    {
      title: t("grade"),
      dataIndex: "class_name",
      key: "class_name",
      width: 94,
      sorter: {},
    },
    {
      title: t("activities"),
      dataIndex: "group_list",
      key: "group_list",
      width: 188,
      render: (name: string, record: any) => (
        <BadgeContainer>
          {record.group_list &&
            record.group_list.length > 0 &&
            record.group_list.map((activity: any, index: number) => (
              <p key={index}>{activity.name}</p>
            ))}
        </BadgeContainer>
      ),
    },
    {
      title: t("attendance"),
      dataIndex: "roll_number",
      key: "roll_number",
      width: 110,
      sorter: {},
    },
    {
      title: t("gender"),
      dataIndex: "gender",
      key: "gender",
      width: 78,
      sorter: {},
      render: (gender: number) => (
        <RowCellWrapper>{gender === 0 ? "男性" : "女性"}</RowCellWrapper>
      ),
    },
    {
      title: t("birthday"),
      dataIndex: "date_of_birth",
      key: "date_of_birth",
      width: 130,
      sorter: {},
    },
    {
      title: "",
      dataIndex: "",
      key: "8",
      width: 250,
    },
  ];

  const onSubmit = () => {
    // submit search function
    const paramsUserList: UserListData = {
      page: currentPage,
      c_department_id: watchClassField ? watchClassField : "",
      user_name: watchSearchNameField
        ? Buffer.from(watchSearchNameField, "utf-8").toString()
        : "",
      locale: currentLanguage,
      per_page:
        watchNumberRecordPerPage !== undefined
          ? watchNumberRecordPerPage
          : PAGE_SIZE,
    };
    dispatch(UserListActionCreators.userListAction(paramsUserList));

    if (errorMessageUserList) window.confirm(errorMessageUserList);
  };

  const handleChangePage = (page: number) => {
    setCurrentPage(page);
  };

  const handleCancel = () => {
    setVisible(false);
    getUserApiCall();
  };

  const handleSubmitOK = () => {
    setVisible(false);
    getUserApiCall();
  };

  const ActionSection = () => {
    return (
      <TaskWrapper>
        <TaskWrapperLeft>
          <Button
            onClick={() => showModal("creating")}
            icon={<PlusIcon fill="currentColor" />}
            name={registerBtnName}
            color="#2AC769"
            background="#FFFFFF"
            border="1px solid #1AB759"
            fontSize={14}
            fontWeight={500}
            padding="0px 5px"
          />
          <Button
            onClick={() => showModal("batch")}
            icon={<PlusIcon fill="currentColor" />}
            name={batchRegisterBtnName}
            color="#2AC769"
            background="#FFFFFF"
            border="1px solid #1AB759"
            fontSize={14}
            fontWeight={500}
            padding="0px 5px"
          />
        </TaskWrapperLeft>
        <TaskWrapperRight>
          <Button
            icon={
              <DownloadIcon
                width="14px"
                height="14px"
                fill="currentColor"
                style={{ position: "absolute", left: 4, top: 6 }}
              />
            }
            name={t("excel-output")}
            background="#2AC769"
            color="#FFFFFF"
            border="none"
            fontSize={12}
            fontWeight={700}
            padding="3px 7px 3px 16px"
          />
          <Dropdown overlay={menu} placement="bottomRight" arrow>
            <MoreIcon />
          </Dropdown>
        </TaskWrapperRight>
      </TaskWrapper>
    );
  };

  const FooterModal = (
    color: string,
    backgroundBtnLeft: string,
    backgroundBtnRight: string,
    padding: string,
    nameBtnLeft: string,
    nameBtnRight: string,
    onCancel?: () => void,
    onOk?: () => void
  ) => {
    const border = `1px solid ${backgroundBtnLeft}`;
    return (
      <WrapperBtnForm>
        <Button
          type="reset"
          color={color}
          fontSize={16}
          fontWeight={700}
          lineHeight="16px"
          background={backgroundBtnLeft}
          padding={padding}
          name={nameBtnLeft}
          border={border}
          onClick={onCancel}
        />
        <Button
          type="submit"
          color={color}
          fontSize={16}
          fontWeight={700}
          lineHeight="16px"
          background={backgroundBtnRight}
          padding={padding}
          name={nameBtnRight}
          border={border}
          onClick={onOk}
        />
      </WrapperBtnForm>
    );
  };

  const ShowPagination = () => {
    return (
      <PaginationWrapper>
        <Pagination
          current={currentPage}
          onChange={handleChangePage}
          pageSize={
            watchNumberRecordPerPage !== undefined
              ? watchNumberRecordPerPage
              : PAGE_SIZE
          }
          total={userlistdata?.total_record}
          showSizeChanger={false}
          showLessItems={!screens.xl}
        />
      </PaginationWrapper>
    );
  };

  const renderForm = () => {
    switch (formStatus) {
      case "updating":
        return (
          visible && (
            <UpdatingModal
              visible={visible}
              handleCancel={handleCancel}
              handleSubmitOK={handleSubmitOK}
              FooterModal={FooterModal}
              activityGroupOptions={activitieslistdata?.result}
              departmentOptions={departmentlistdata?.result}
              genderOptions={genderOptions}
              selectedUserItem={selectedItem}
            />
          )
        );
      case "creating":
        return (
          visible && (
            <CreatingModal
              visible={visible}
              handleCancel={handleCancel}
              FooterModal={FooterModal}
              activityGroupOptions={activitieslistdata?.result}
              departmentOptions={departmentlistdata?.result}
              genderOptions={genderOptions}
            />
          )
        );
      case "attendance":
        return (
          <AttendanceModal visible={visible} handleCancel={handleCancel} />
        );
      case "batch":
        return (
          <BatchModal
            visible={visible}
            handleCancel={handleCancel}
            FooterModal={FooterModal}
          />
        );
      default:
        return;
    }
  };

  return (
    <>
      {renderForm()}
      <Box
        title={t("user-management")}
        subTitle={`${t("total-number-of-people")}
        ${userlistdata?.total_record}人`}
        padding="24px 16px"
        action={
          <div style={{ display: "flex", alignItems: "center" }}>
            <div
              style={{
                fontSize: 14,
                fontWeight: 500,
                marginRight: 5,
              }}
            >
              {t("record")}
            </div>
            <Controller
              control={control}
              name="number_record"
              render={({ field: { onChange } }) => (
                <Select
                  defaultValue="25件"
                  options={rightOptions}
                  onChange={onChange}
                />
              )}
            />
          </div>
        }
      >
        <SearchWrapper>
          <form onSubmit={handleSubmit(onSubmit)}>
            <SearchWrapperLeft>
              <Controller
                control={control}
                name="name"
                render={({ field: { onChange } }) => (
                  <Input height={31} onChange={onChange} />
                )}
              />

              <WrapperSelectAndBtn>
                <Controller
                  control={control}
                  name="grade"
                  render={({ field: { onChange } }) => (
                    <Select
                      placeholder={t("select-grade-or-class")}
                      options={classlistdata?.result}
                      onChange={onChange}
                    />
                  )}
                />

                <Button
                  type="submit"
                  name={t("search")}
                  background="#2AC769"
                  color="#FFFFFF"
                  border="none"
                  fontSize={16}
                  fontWeight={700}
                  padding="1px 16px"
                />
              </WrapperSelectAndBtn>
            </SearchWrapperLeft>
          </form>

          <ShowPagination />
        </SearchWrapper>

        <ContentWrapper>
          <TableStyled
            dataSource={userlistdata?.result}
            columns={columns}
            actionSection={<ActionSection />}
            scroll={!userlistdata?.result ? {} : { x: "max-content", y: 710 }}
            onRow={(record: UserListResultData) => ({
              onClick: () => {
                setSelectedItem(record);
              },
            })}
          />
        </ContentWrapper>
        <ShowPagination />
      </Box>
    </>
  );
};

export default UserList;
