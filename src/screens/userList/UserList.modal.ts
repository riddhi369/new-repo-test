import { IOption } from "components/select/Select";
import { ActivitesResultData } from "models/activities";
import { UserListResultData } from "models/userlist";

export interface IModal {
  visible?: boolean;
  handleCancel: () => void;
  handleSubmitOK?: () => void;
  FooterModal?: any;
  activityGroupOptions?: ActivitesResultData;
  genderOptions?: IOption[];
  departmentOptions?: IOption[];
  selectedUserItem?: UserListResultData;
}
