import React, { useEffect } from "react";
import { Row, Col, TreeSelect } from "antd";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { useTranslation } from "react-i18next";
import {
  Input as Input,
  Select,
  Modal,
  DatePicker,
  Button,
  TreeSelectCustom,
} from "components";
import {
  DeleteButton,
  WrapperBtnUpdatingForm,
  GroupBtn,
  Label,
  ImportantStyled,
} from "./UserList.style";
import { IModal } from "./UserList.modal";
import { IGroupListOption } from "models/activities";
import { localStorageHelper } from "utils";
import { SubDepartmentListData } from "models/subdepartmentList";
import { subDepartmentListDataResponse } from "redux/subdepartment/subdepartmentListStates";
import { useDispatch, useSelector } from "react-redux";
import { UpdateUserData } from "models/updateuser";
import {
  UpdateUserActionCreators,
  SubDepartmentListActionCreators,
  DeleteUserActionCreators,
} from "redux/rootActions";
import {
  updateUserSuccessResponse,
  updateUserErrorMessage,
} from "redux/updateuser/updateuserStates";
import {
  deleteUserSuccessResponse,
  deleteUserErrorMessage,
} from "redux/deleteuser/deleteuserStates";

const { TreeNode } = TreeSelect;

const schema = yup.object().shape({
  lastName: yup.string().trim().required("last-name-required"),
  firstName: yup.string().trim().required("first-name-required"),
});

const UpdatingModal = ({
  visible,
  handleCancel,
  handleSubmitOK,
  activityGroupOptions,
  genderOptions,
  departmentOptions,
  selectedUserItem,
}: IModal) => {
  const { t }: any = useTranslation();
  const currentLanguage = localStorageHelper.getItem("i18nextLng");
  const dispatch = useDispatch();
  const subdepartmentOptions = useSelector(subDepartmentListDataResponse);
  const [activities, setActivities] = React.useState<IGroupListOption[]>();
  const updateUserSuccess = useSelector(updateUserSuccessResponse);

  const deleteUserSuccess = useSelector(deleteUserSuccessResponse);
  const deleteUserError = useSelector(deleteUserErrorMessage);

  const updateUserError = useSelector(updateUserErrorMessage);
  const { watch, handleSubmit, control } = useForm({
    reValidateMode: "onChange",
    defaultValues: {
      lastName: selectedUserItem?.last_name,
      firstName: selectedUserItem?.first_name,
      lastNameFurigana: selectedUserItem?.furi_last_name,
      firstNameFurigana: selectedUserItem?.furi_first_name,
      gender: selectedUserItem?.gender === 0 ? "男性" : "女性",
      birthDay:
        selectedUserItem && selectedUserItem.date_of_birth
          ? new Date(selectedUserItem?.date_of_birth.toString())
          : new Date(),
      attendance: selectedUserItem?.roll_number,
      schoolYear: departmentOptions?.map((item) => {
        if (item.id === selectedUserItem?.department_id) return item.name;
      }),
      class: subdepartmentOptions?.result?.map((item) => {
        if (item.id === selectedUserItem?.c_department_id) return item.name;
      }),
      activities: [],
    },
    resolver: yupResolver(schema),
  });
  const watchSchoolYearField = watch("schoolYear");
  const watchFirstName = watch("firstName");
  const watchLastName = watch("lastName");

  useEffect(() => {
    setActivities(
      activityGroupOptions && activityGroupOptions?.group_list !== undefined
        ? activityGroupOptions?.group_list
        : []
    );
  }, []);

  useEffect(() => {
    if (selectedUserItem && selectedUserItem.department_id !== undefined) {
      const subDepartmentparams: SubDepartmentListData = {
        locale: currentLanguage,
        department_id: selectedUserItem.department_id,
        role: 1,
      };
      dispatch(
        SubDepartmentListActionCreators.subDepartmentListAction(
          subDepartmentparams
        )
      );
    }
  }, [selectedUserItem?.department_id]);

  useEffect(() => {
    if (watchSchoolYearField) {
      const subDepartmentparams: SubDepartmentListData = {
        locale: currentLanguage,
        department_id: watchSchoolYearField,
        role: 1,
      };
      dispatch(
        SubDepartmentListActionCreators.subDepartmentListAction(
          subDepartmentparams
        )
      );
    }
  }, [watchSchoolYearField]);

  const onSubmit = (data: any, e: any) => {
    e.preventDefault();
    const updateUserParams: UpdateUserData = {
      company_user: {
        id: selectedUserItem?.id,
        first_name: data.firstName,
        last_name: data.lastName,
        furi_first_name: data.firstNameFurigana,
        furi_last_name: data.lastNameFurigana,
        gender: data.gender === "男性" ? 0 : 1,
        date_of_birth: data.birthDay,
        dept_id: data.schoolYear,
        sub_dept_id: data.class,
        roll_number: data.attendance,
        company_sub_department_id: data.activities,
      },
      locale: currentLanguage,
    };
    dispatch(UpdateUserActionCreators.createUserAction(updateUserParams));
    if (updateUserSuccess?.message) {
      window.confirm(updateUserSuccess.message);
      handleCancel();
    } else {
      window.confirm(updateUserError);
    }
  };

  const onDelete = () => {
    if (selectedUserItem) {
      dispatch(
        DeleteUserActionCreators.deleteUserAction({ id: selectedUserItem.id })
      );
      if (deleteUserSuccess?.message) {
        window.confirm(deleteUserSuccess.message);
        handleCancel();
      } else {
        window.confirm(deleteUserError);
      }
    }
  };

  return (
    <Modal
      type="form"
      title={t("user-information-editing")}
      visible={visible}
      onCancel={handleCancel}
      onOk={handleSubmit(onSubmit)}
    >
      <form onSubmit={handleSubmit(onSubmit)}>
        <Row gutter={16}>
          <Col xs={12} xl={12}>
            <Controller
              control={control}
              name="lastName"
              render={({
                field: { onChange, value },
                fieldState: { error },
              }) => (
                <Input
                  value={value}
                  label={
                    <>
                      {t("last-name")}
                      <ImportantStyled>*</ImportantStyled>
                    </>
                  }
                  height={31}
                  fsLabel={16}
                  marginForm="0 0 22px 0"
                  onChange={onChange}
                  error={t(error?.message)}
                />
              )}
            />
          </Col>
          <Col xs={12} xl={12}>
            <Controller
              control={control}
              name="firstName"
              render={({
                field: { onChange, value },
                fieldState: { error },
              }) => (
                <Input
                  value={value}
                  label={
                    <>
                      {t("first-name")}
                      <ImportantStyled>*</ImportantStyled>
                    </>
                  }
                  height={31}
                  fsLabel={16}
                  marginForm="0 0 22px 0"
                  onChange={onChange}
                  error={t(error?.message)}
                />
              )}
            />
          </Col>
        </Row>

        <Row gutter={16}>
          <Col xs={12} xl={12}>
            <Controller
              control={control}
              name="lastNameFurigana"
              render={({
                field: { onChange, value },
                fieldState: { error },
              }) => (
                <Input
                  value={value}
                  label={t("last-name-furigana")}
                  height={31}
                  fsLabel={16}
                  marginForm="0 0 22px 0"
                  onChange={onChange}
                  error={t(error?.message)}
                />
              )}
            />
          </Col>
          <Col xs={12} xl={12}>
            <Controller
              control={control}
              name="firstNameFurigana"
              render={({
                field: { onChange, value },
                fieldState: { error },
              }) => (
                <Input
                  value={value}
                  label={t("first-name-furigana")}
                  height={31}
                  fsLabel={16}
                  marginForm="0 0 22px 0"
                  onChange={onChange}
                  error={t(error?.message)}
                />
              )}
            />
          </Col>
        </Row>

        <Row gutter={16}>
          <Col xs={12} md={8} xl={8}>
            <Controller
              control={control}
              name="gender"
              render={({
                field: { onChange, value },
                fieldState: { error },
              }) => (
                <Select
                  value={value}
                  label={t("gender")}
                  placeholder={t("please-select")}
                  options={genderOptions}
                  fsLabel={16}
                  marginForm="0 0 22px 0"
                  onChange={onChange}
                  error={t(error?.message)}
                />
              )}
            />
          </Col>
          <Col xs={12} md={8} xl={8}>
            <Controller
              control={control}
              name="birthDay"
              render={({
                field: { onChange, value },
                fieldState: { error },
              }) => (
                <DatePicker
                  selected={value}
                  onChange={onChange}
                  error={t(error?.message)}
                  label={t("birthday")}
                />
              )}
            />
          </Col>
          <Col xs={12} md={8} xl={8}>
            <Controller
              control={control}
              name="attendance"
              render={({
                field: { onChange, value },
                fieldState: { error },
              }) => (
                <Input
                  value={value}
                  label={t("attendance")}
                  height={31}
                  fsLabel={16}
                  marginForm="0 0 22px 0"
                  onChange={onChange}
                  error={t(error?.message)}
                />
              )}
            />
          </Col>
        </Row>

        <Row gutter={16}>
          <Col xs={24} md={12} xl={12}>
            <Controller
              control={control}
              name="schoolYear"
              render={({
                field: { onChange, value },
                fieldState: { error },
              }) => (
                <Select
                  value={value}
                  label={
                    <>
                      {t("school-year")}
                      <ImportantStyled>*</ImportantStyled>
                    </>
                  }
                  placeholder={t("please-select")}
                  options={departmentOptions}
                  fsLabel={16}
                  marginForm="0 0 22px 0"
                  onChange={onChange}
                  error={t(error?.message)}
                />
              )}
            />
          </Col>
          <Col xs={24} md={12} xl={12}>
            <Controller
              control={control}
              name="class"
              render={({
                field: { onChange, value },
                fieldState: { error },
              }) => (
                <Select
                  value={value}
                  label={
                    <>
                      {t("class")}
                      <ImportantStyled>*</ImportantStyled>
                    </>
                  }
                  placeholder={t("please-select")}
                  options={subdepartmentOptions?.result}
                  fsLabel={16}
                  disabled={
                    selectedUserItem?.department_id !== undefined ? false : true
                  }
                  marginForm="0 0 22px 0"
                  onChange={onChange}
                  error={t(error?.message)}
                />
              )}
            />
          </Col>
        </Row>

        <Label>{t("activities")}</Label>
        {activityGroupOptions?.compulsory_group ? (
          <Label>
            <ImportantStyled>
              下記のグループの選択は必須です :
              {activityGroupOptions?.compulsory_group}
            </ImportantStyled>
          </Label>
        ) : (
          ""
        )}
        <Controller
          control={control}
          name="activities"
          render={({ field: { onChange, value }, fieldState: { error } }) => (
            <TreeSelectCustom
              error={t(error?.message)}
              placeholder={t("please-select-activities")}
              value={value}
              onChange={onChange}
            >
              {activities &&
                activities.length > 0 &&
                activities.map((it: IGroupListOption) => (
                  <>
                    <TreeNode
                      value={it.text}
                      title={it.text}
                      disabled={
                        it.children && it.children.length > 0 ? true : false
                      }
                    >
                      {it.children &&
                        it.children.length > 0 &&
                        it.children.map((child: IGroupListOption) => (
                          <>
                            <TreeNode
                              value={child.text}
                              title={child.text}
                            ></TreeNode>
                          </>
                        ))}
                    </TreeNode>
                  </>
                ))}
            </TreeSelectCustom>
          )}
        />
        <WrapperBtnUpdatingForm>
          <GroupBtn>
            <Button
              type="reset"
              color="#FFFFFF"
              fontSize={16}
              fontWeight={700}
              lineHeight="16px"
              background="#E0E0E0"
              padding="11px 24px 11px"
              name={t("cancel")}
              border="none"
              onClick={handleCancel}
            />
            <Button
              type="submit"
              color="#FFFFFF"
              fontSize={16}
              fontWeight={700}
              lineHeight="16px"
              background="#2AC769"
              padding="11px 24px 11px"
              name={t("register")}
              border="none"
              onClick={() => handleSubmitOK}
            />
          </GroupBtn>
          <DeleteButton onClick={onDelete}>このユーザーを削除する</DeleteButton>
        </WrapperBtnUpdatingForm>
      </form>
    </Modal>
  );
};

export default UpdatingModal;
