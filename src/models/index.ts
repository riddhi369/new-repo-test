export interface IResponse {
  message: string;
  status: 200 | 400 | 401 | 403 | 404;
}
