import { createGlobalStyle } from "styled-components";
import "antd/dist/antd.css";
import { theme } from "./theme";

export const GlobalStyle = createGlobalStyle`
    html, body {
        margin: 0;
        padding: 0;
        font-family: 'Noto Sans JP', sans-serif;
        background: #f2f2f2;
    }
    * {
        box-sizing: border-box;
    }
    p {
        margin-bottom: 0;
    }

    .ant-select-dropdown {
        border-radius: 6px!important;
        border: 1px solid ${theme.colors.border};
        box-shadow: 0px 4px 16px rgba(0, 0, 0, 0.08) !important;
    }
    .ant-select-item {
        padding: 12px 16px 14px;
        font-size: 12px;
        font-weight: 700;
        line-height: 16px;
        border-bottom: 1px solid ${theme.colors.background};
    }
    .ant-select-item:last-child {
        border-bottom: unset;
    }
    .ant-picker-dropdown {
        width: 300px;
        position: unset;
        z-index: unset;
    }

    .ant-dropdown-menu {
        border-radius: 6px;
    }
    .ant-drawer-content-wrapper {
        width: 320px!important;
        ${theme.media.mobileM} {
            width: 375px!important;
        }
    }    
    .ant-drawer-right {
        top: 60px;
    }
    .ant-drawer-close {
        display: none;
    }
    .ant-drawer-header {
        border-bottom: 4px solid ${theme.colors.textFieldBackground};
        padding: 16px 30px;
    }
    .ant-drawer-body {
        padding: 25px 0px 77px;
    }

    .ant-menu {
        background: ${theme.colors.white};
        font-size: ${theme.sizes.xs}
    }
    .ant-menu-item {
        font-weight: ${theme.fontWeight.medium};
        font-size: ${theme.sizes.xs};
        line-height: 17px;
        color: ${theme.colors.text.primary};
        margin: 0px;
        margin-bottom: 0px!important;
        height: 50px;
        padding-left: 30px!important;
    }
    .ant-menu-item-selected {
        svg {
            fill: ${theme.colors.textLink}!important;
        }
        a {
            color: ${theme.colors.textLink}!important;
            font-weight: ${theme.fontWeight.bold};
        }
    }
    .ant-menu:not(.ant-menu-horizontal) .ant-menu-item-selected {
        background: unset;
    }
    .ant-menu-inline .ant-menu-item::after {
        border-right: unset;
    }
    .ant-menu-light .ant-menu-item svg {
        fill: ${theme.colors.text.primary};
    }
    .ant-menu-light .ant-menu-item:hover {
        background: #ecf5fb;
        a {
            color: ${theme.colors.text.primary}
        }
    }
    .ant-menu-light .ant-menu-submenu-title:hover {
        color: ${theme.colors.text.primary};
        background: #ecf5fb;
        svg {
            fill: ${theme.colors.text.primary};
        }
    }
    .ant-menu-title-content {
        transition: unset;
        font-weight: ${theme.fontWeight.medium};
    }
    .ant-menu-item .ant-menu-item-icon + span{
        transition: unset;
    }
    .ant-menu-submenu svg {
        fill: ${theme.colors.text.primary};
    }
    .ant-menu-sub li span {
        padding-left: 19px;
    }
    .ant-menu-sub {
        padding-top: 0!important;
        border-right: unset;
    }
    .ant-menu-item-only-child {
        padding-left: 54px!important;
    }
    t-menu-submenu-title {
        padding-left: 22px!important;
    }
    .ant-menu-submenu:hover > .ant-menu-submenu-title > .ant-menu-submenu-arrow {
        color: ${theme.colors.text.primary};
    }
    .ant-menu-inline {
        border-right: unset!important;
        background: unset!important;
    }
    .ant-menu-submenu-title {
        padding-left: 30px!important;
    }

    .ant-select-tree-title {
        font-weight: ${theme.fontWeight.bold};
        font-size: ${theme.sizes.xs};
        line-height: 17px;
    }
    .ant-select-tree-node-selected {
        background-color: #e6f7ff!important;
    }
`;
