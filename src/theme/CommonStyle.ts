import styled, { DefaultTheme } from "styled-components";

export const ErrorMessage = styled.p(({ theme }) => ({
  color: theme.colors.error,
  fontSize: theme.sizes.xs,
  fontWeight: theme.fontWeight.medium,
  lineHeight: "12px",
  position: "absolute",
  bottom: -15,
}));

export const LinkStyled = (theme: DefaultTheme) => {
  return {
    fontSize: theme.sizes.xs,
    fontWeight: theme.fontWeight.medium,
    lineHeight: "12px",
    color: theme.colors.textLink,
    textDecoration: "underline",
    cursor: "pointer",
  };
};
