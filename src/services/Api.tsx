import { isLoggedIn, getCookie } from "utils";

export const apiBaseRoute = process.env.REACT_APP_API_URL;
export const authTokenKey = process.env.REACT_APP_ACCESS_TOKEN_KEY!;

export enum MethodType {
  POST = "POST",
  GET = "GET",
  PUT = "PUT",
  DELETE = "DELETE",
}

export const URI = {
  AUTH: {
    LOGIN: "/web_api_v1/users/login",
    FORGOTPASSWORD: "/web_api_v1/users/forgot_password",
    RESETPASSWORD: "/web_api_v1/users/password_reset",
  },
  DASHBOARD: {
    GETUSERLIST: "/web_api_v1/company_users",
    GETCLASSLIST: "/web_api_v1/company_users/class_list",
    GETDEPARTMENTLIST: "/web_api_v1/company_users/department_list",
    GETSUBDEPARTMENTLIST: "/web_api_v1/company_users/department_list",
    GET_ACTIVITY_GROUP_LIST: "/web_api_v1/company_users/group_list",
    CREATE_USER: "/web_api_v1/company_users",
    DELETE_USER: "/web_api_v1/company_users/delete",
  },
};

export const createApiCall = async ({
  method = "GET",
  url = "",
  data = {},
}) => {
  const headers: any = {
    "Content-Type": "application/json",
  };
  if (isLoggedIn()) {
    headers["X-USER-TOKEN"] = getCookie(authTokenKey);
  }

  if (method === "GET" || method === "DELETE") {
    return fetch(
      `${apiBaseRoute}${url}` + "?" + new URLSearchParams(data).toString(),
      {
        body: undefined,
        cache: "no-cache",
        headers,
        method,
      }
    )
      .then((response) => response.json())
      .then((result) => result)
      .catch((error) => {
        return error;
      });
  } else {
    return fetch(`${apiBaseRoute}${url}`, {
      body: JSON.stringify(data),
      cache: "no-cache",
      headers,
      method,
    })
      .then((response) => response.json())
      .then((result) => result)
      .catch((error) => {
        return error;
      });
  }
};
