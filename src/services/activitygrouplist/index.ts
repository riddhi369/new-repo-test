import { createApiCall, MethodType, URI } from "../Api";
import { ActivitiesListData } from "models/activities";

export const fetchActivites = (payload: ActivitiesListData) => {
  return createApiCall({
    method: MethodType.GET,
    url: URI.DASHBOARD.GET_ACTIVITY_GROUP_LIST,
    data: payload,
  });
};
