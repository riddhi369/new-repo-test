import { createApiCall, MethodType, URI } from "../Api";
import { ResetPasswordData } from "models/resetPassword";

export const fetchResetPassword = (payload: ResetPasswordData) => {
  return createApiCall({
    method: MethodType.POST,
    url: URI.AUTH.RESETPASSWORD,
    data: payload,
  });
};
