import { createApiCall, MethodType, URI } from "../Api";
import { ForgotPasswordData } from "models/forgotPassword";

export const fetchForgotPassword = (payload: ForgotPasswordData) => {
  return createApiCall({
    method: MethodType.PUT,
    url: URI.AUTH.FORGOTPASSWORD,
    data: payload,
  });
};
