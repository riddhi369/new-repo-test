import { createApiCall, MethodType, URI } from "../Api";
import { LoginData } from "models/authentication";

export const fetchLogin = (payload: LoginData) => {
  return createApiCall({
    method: MethodType.POST,
    url: URI.AUTH.LOGIN,
    data: payload,
  });
};
