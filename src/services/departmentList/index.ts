import { createApiCall, MethodType, URI } from "../Api";
import { DepartmentListData } from "models/departmentList";

export const fetchDepartmentList = (payload: DepartmentListData) => {
  return createApiCall({
    method: MethodType.GET,
    url: URI.DASHBOARD.GETDEPARTMENTLIST,
    data: payload,
  });
};
