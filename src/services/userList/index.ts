import { createApiCall, MethodType, URI } from "../Api";
import { UserListData } from "models/userlist";
import { CreateUserData } from "models/createuserlist";
import { UpdateUserData } from "models/updateuser";
import { DeleteUserData } from "models/deleteuser";

export const fetchUserList = (payload: UserListData) => {
  return createApiCall({
    method: MethodType.GET,
    url: URI.DASHBOARD.GETUSERLIST,
    data: payload,
  });
};

export const createNewUser = (payload: CreateUserData) => {
  return createApiCall({
    method: MethodType.POST,
    url: URI.DASHBOARD.CREATE_USER,
    data: payload,
  });
};

export const updateUser = (payload: UpdateUserData) => {
  return createApiCall({
    method: MethodType.PUT,
    url: URI.DASHBOARD.CREATE_USER,
    data: payload,
  });
};

export const deleteUser = (payload: DeleteUserData) => {
  return createApiCall({
    method: MethodType.DELETE,
    url: URI.DASHBOARD.DELETE_USER,
    data: payload,
  });
};
