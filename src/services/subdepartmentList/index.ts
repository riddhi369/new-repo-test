import { createApiCall, MethodType, URI } from "../Api";
import { SubDepartmentListData } from "models/subdepartmentList";

export const fetchSubDepartmentList = (payload: SubDepartmentListData) => {
  return createApiCall({
    method: MethodType.GET,
    url: URI.DASHBOARD.GETSUBDEPARTMENTLIST,
    data: payload,
  });
};
