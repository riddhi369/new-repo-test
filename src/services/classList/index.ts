import { createApiCall, MethodType, URI } from "../Api";
import { ClassListData } from "models/classList";

export const fetchClassList = (payload: ClassListData) => {
  return createApiCall({
    method: MethodType.GET,
    url: URI.DASHBOARD.GETCLASSLIST,
    data: payload,
  });
};
