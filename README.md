# leber_organization_console

## Introduce

React JS LEBER organisation console

## Environment

Include: [.env, .env.production] files

Format:

```
REACT_APP_API_URL=''
REACT_APP_ACCESS_TOKEN_KEY=''
```

## Run project

1.Install NPM packages

```bash
npm install
# or
yarn
```

2.Run locally

```bash
npm run start
# or
yarn start
```

## IDE

Tools for development application

| Name               | Website                                  |
| ------------------ | ---------------------------------------- |
| Visual Studio Code | [https://code.visualstudio.com/download] |

## Structure

LEBER organisation console structure in folder src:
| Name | Usage |
| ------ | ------ |
| components | Contain components |
| configs | Contain configs |
| hooks | Contain hooks |
| icons | Contain icons |
| layouts | Contain layouts |
| models | Contain interfaces, types |
| routes | Define routes |
| screens | Contain pages of project |
| theme | Contain global style |
| utils | Contain common functions |

## License
